import { useState, useRef, useEffect } from "react"

export const board = { w: 300, h: 300 }
const ball = { w: 24, h: 24 }
const interval = 0.05  // seconds
const kp = 0.6, rotateMax = 5, rotateMin = -5

export default function Board() {
  const [rX, setRX] = useState(0)
  const [rY, setRY] = useState(0)
  const rotate = useRef<any>({})

  const [x, setX] = useState(board.w / 2)
  const [y, setY] = useState(board.h / 2)
  const position = useRef<any>({})

  useEffect(() => {
    rotate.current.x = rX
    rotate.current.y = rY
  }, [rX, rY])

  useEffect(() => {
    position.current.x = x
    position.current.y = y
  }, [x, y])

  useEffect(() => {
    const ballId = setInterval(handleBall, interval * 1000)
    const pidId = setInterval(handlePid, 100)
    return () => {
      clearInterval(ballId)
      clearInterval(pidId)
    }
  }, [])

  const handleBall = () => {
    const newX = 20 * rotate.current.x * interval
    const newY = 20 * rotate.current.y * interval
    setX(prev => {
      if (prev > board.w) return prev
      return prev + newX
    })
    setY(prev => {
      if (prev > board.h) return prev
      return prev + newY
    })
  }

  const handlePid = () => {
    const center = { x: board.w / 2, y: board.h / 2 }
    const error = {
      x: center.x - position.current.x,
      y: center.y - position.current.y,
    }
    let pidX = kp * error.x
    if (pidX > rotateMax) pidX = rotateMax
    if (pidX < rotateMin) pidX = rotateMin
    setRX(pidX)

    let pidY = kp * error.y
    if (pidY > rotateMax) pidY = rotateMax
    if (pidY < rotateMin) pidY = rotateMin
    setRY(pidY)
  }

  const handleClick = (e: any) => {
    setX(e.clientX)
    setY(e.clientY)
  }

  return (
    <>
      <div
        style={{ display: "flex", alignItems: "center" }}
      >
        <div
          style={{
            border: "1px solid gray",
            width: board.w,
            height: board.h,
            position: "relative",
          }}
          onClick={handleClick}
        >
          {/* Ball */}
          <div
            style={{
              width: ball.w,
              height: ball.h,
              borderRadius: "50%",
              background: "lightgray",
              position: "absolute",
              left: x - ball.w / 2,
              top: y - ball.h / 2,
            }}
          >
          </div>
        </div>

        <button onClick={e => setRX(prev => prev + 2)}>
          { rX }
        </button>
      </div>
      <button
        style={{ marginLeft: 140 }}
        onClick={e => setRY(prev => prev + 2)}
      >
        { rY }
      </button>
    </>
  )
}
