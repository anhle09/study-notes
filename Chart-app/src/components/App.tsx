import { useState } from "react"
import CurrentView from "./CurrentView"
import HistoryView from "./HistoryView"

export default function App() {
    const [mode, setMode] = useState('current')

    return (
        <>
            <div style={{ marginTop: 20, marginBottom: -10 }}>
                <select value={mode} onChange={(e) => setMode(e.target.value)}>
                    <option value="current">Current</option>
                    <option value="history">History</option>
                </select>
            </div>
            {mode === 'current' ? <CurrentView /> : <HistoryView />}
        </>
    )
}

/** feature

page 1: sort by signal (one day, multiple symbol)
- ui: resolution, date input
- compare close vs open -> red stick
- trend, resistance (manual)
- expect: %, all green, some red, start < middle < end
- sort, remove

page 2: back test (one symbol, multiple day)
- ui: symbol input
- calculate pre-trend, sort
- compare first, middle, end day -> profit

** merge 5' (9h30 - 10h) -> 1h (15h) -> require 12 candle

** problem:
- finhub data != tradingview data

c8e4s8aad3i91d97edkg
4-20h

*/
