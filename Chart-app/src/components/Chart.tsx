import { Chart } from 'react-google-charts'
import { IChartSummary } from '../utils/chartData'

const green = '#0f9d58', red = '#c32c15'

const options = {
    legend: 'none',
    bar: { groupWidth: '40%' }, // candle width
    candlestick: {
        fallingColor: { strokeWidth: 0, fill: red },
        risingColor: { strokeWidth: 0, fill: green },
      },
}

interface ICandleChart extends IChartSummary {
    onRemove?: (symbol: string) => void
}

export default function CandleChart(props: ICandleChart) {
    if (!props.chartData?.length) return <h4>No data</h4>
    const { symbol, trend, percent, profit, onRemove } = props

    return (
        <section>
            <div className='flex align-center'>
                <h4 style={{ margin: 0 }} className='flex-grow'>
                    <span>{`** ${symbol} `}</span>
                    {trend && <span>{`(${trend}):`}</span>}
                    <span style={{ color: percent > 0 ? green : red }}>
                        {` ${percent} | ${profit?.join(' | ')}`}
                    </span>
                </h4>
                {onRemove && (
                    <button
                        onClick={() => onRemove(symbol)}
                        style={{ marginRight: 60, fontSize: 12 }}
                    >Remove
                    </button>
                )}
            </div>

            <Chart
                chartType='CandlestickChart'
                width='400px'
                height='360px'
                data={props.chartData}
                options={options}
            />
        </section>
    )
}
