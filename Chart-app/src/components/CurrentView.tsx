import { useState } from 'react'
import { DateTime } from 'luxon'
import { getChartList, IChartSummary } from '../utils/chartData'
import { getRemovedSymbol, getSymbolList, storeRemovedSymbol } from '../utils/symbolTrend'
import { getUsTimeNow } from '../utils/time'
import CandleChart from './Chart'

export default function CurrentView() {
    const [date, setDate] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [expand1h, setExpand1h] = useState(false)
    const [allSymbol, setAllSymbol] = useState(false)
    const [sorted, setSorted] = useState(false)
    const [chartList, setChartList] = useState<IChartSummary[]>()

    const fetchData = () => {
        if (!date) return alert('Please select date')
        const symbolList = getSymbolList(date, allSymbol)
        if (!symbolList) return alert('Trend data is invalid')

        setIsLoading(true)
        getChartList({ date, symbolList, expand1h, sorted })
            .then(res => setChartList(res))
            .catch(err => alert(err.message))
            .finally(() => setIsLoading(false))
    }

    const handleRemove = (symbol: string) => {
        const newList = chartList?.filter(item => item.symbol !== symbol)
        setChartList(newList)
        storeRemovedSymbol(symbol, date)
    }

    const usTime = getUsTimeNow().toLocaleString(DateTime.DATETIME_MED)
    const removedSymbol = getRemovedSymbol(date)

    return (
        <>
            <div className='flex align-center'>
                <h4>{ usTime }</h4>

                <label style={{ marginLeft: '2em' }}>
                    <input type='checkbox' onClick={() => setExpand1h(prev => !prev)} />
                    {' '} Expand 1h
                </label>

                <label style={{ marginLeft: '2em' }}>
                    <input type='checkbox' onClick={() => setSorted(prev => !prev)} />
                    {' '} Sorted
                </label>

                <label style={{ marginLeft: '2em' }}>
                    <input type='checkbox' onClick={() => setAllSymbol(prev => !prev)} />
                    {' '} All symbol
                </label>

                <span style={{ marginLeft: '2em' }}>
                    <input type='date' onChange={e => setDate(e.target.value)} />
                </span>

                <span style={{ marginLeft: '2em' }}>
                    <button onClick={fetchData}>{isLoading ? 'Fetching...' : 'Fetch'}</button>
                </span>

                {removedSymbol && (
                    <span style={{ marginLeft: '2em' }}>
                        Removed: {removedSymbol.join(', ')}
                    </span>
                )}
            </div>

            <main className='flex flex-wrap'>
                {chartList?.map(item => (
                    <CandleChart
                        key={item.symbol}
                        onRemove={handleRemove}
                        {...item}
                    />
                ))}
            </main>
        </>
    )
}
