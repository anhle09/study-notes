import { useState } from 'react'
import { DateTime } from 'luxon'
import { getHistoryList, IChartSummary } from '../utils/chartData'
import { getUsTimeNow } from '../utils/time'
import CandleChart from './Chart'

export default function HistoryView() {
    const [date, setDate] = useState('')
    const [symbol, setSymbol] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const [expand1h, setExpand1h] = useState(false)
    const [sorted, setSorted] = useState(false)
    const [chartList, setChartList] = useState<IChartSummary[]>()

    const fetchData = () => {
        if (!date || !symbol) return alert('Please enter symbol and date')
        setIsLoading(true)
        getHistoryList({ date, symbol, expand1h, sorted })
            .then(res => setChartList(res))
            .catch(err => alert(err.message))
            .finally(() => setIsLoading(false))
    }

    const usTime = getUsTimeNow().toLocaleString(DateTime.DATETIME_MED)

    return (
        <>
            <div className='flex align-center'>
                <h4>{ usTime }</h4>

                <label style={{ marginLeft: '2em' }}>
                    <input type='checkbox' onClick={() => setExpand1h(prev => !prev)} />
                    {' '} Expand 1h
                </label>

                <label style={{ marginLeft: '2em' }}>
                    <input type='checkbox' onClick={() => setSorted(prev => !prev)} />
                    {' '} Sorted
                </label>

                <span style={{ marginLeft: '2em' }}>
                    <input
                        type='text' placeholder='Symbol' style={{ width: '98px' }}
                        onChange={e => setSymbol(e.target.value)} value={symbol}
                    />
                </span>

                <span style={{ marginLeft: '2em' }}>
                    <input type='date' onChange={e => setDate(e.target.value)} />
                </span>

                <span style={{ marginLeft: '2em' }}>
                    <button onClick={fetchData}>{isLoading ? 'Fetching...' : 'Fetch'}</button>
                </span>
            </div>

            <main className='flex flex-wrap'>
                {chartList?.map(({ symbol, ...item }) => (
                    <CandleChart
                        key={item.date}
                        symbol={item.date}
                        {...item}
                    />
                ))}
            </main>
        </>
    )
}
