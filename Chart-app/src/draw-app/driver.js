import { gcodeToObject } from 'gcode-json-converter'

const r1 = 100, r2 = 150
export const points = [
  { x: 100, y: 0, radius: 8, label: '0' },
  { x: 100, y: 100, radius: 8, label: '1' },
  { x: 0, y: 0, radius: 8, label: '2' },
  { x: 200, y: 100, radius: 8, label: '3' },
  { x: 200, y: 0, radius: 8, label: '4' }
]
Object.assign(points[2], getIntersection(points[1], points[3]))

// draw size ~ 100 * 100 (r = 150)
// { x: 100, y: 120 } -> { x: 200, y: 220 }
const offset = {
  gcode: { x: 90, y: 80 },
  board: { x: 100, y: 120 },
}
const gcode =
`G1 X10.83 Y8.18 F3500.00
G1 X9.36 Y8.18 F3500.00
G1 X9.36 Y18.11 F3500.00
G1 X10.95 Y18.11 F3500.00
G1 X10.95 Y16.50 F3500.00
G1 X12.09 Y17.44 F3500.00
G1 X13.52 Y17.76 F3500.00
G1 X15.15 Y17.41 F3500.00
G1 X16.42 Y16.43 F3500.00
G1 X17.20 Y14.91 F3500.00
G1 X17.48 Y13.00 F3500.00
G1 X17.19 Y10.87 F3500.00
G1 X16.29 Y9.28 F3500.00
G1 X14.98 Y8.30 F3500.00
G1 X13.44 Y7.97 F3500.00
G1 X11.96 Y8.31 F3500.00
G1 X10.83 Y9.35 F3500.00
G1 X10.83 Y8.18 F3500.00
G1 X10.82 Y12.93 F3500.00
G1 X10.93 Y11.48 F3500.00
G1 X11.28 Y10.50 F3500.00
G1 X12.16 Y9.58 F3500.00
G1 X13.30 Y9.27 F3500.00
G1 X14.27 Y9.50 F3500.00
G1 X15.10 Y10.18 F3500.00
G1 X15.67 Y11.30 F3500.00
G1 X15.86 Y12.87 F3500.00
G1 X15.68 Y14.47 F3500.00
G1 X15.13 Y15.58 F3500.00
G1 X14.33 Y16.23 F3500.00
G1 X13.37 Y16.45 F3500.00
G1 X12.40 Y16.23 F3500.00
G1 X11.58 Y15.55 F3500.00
G1 X11.01 Y14.44 F3500.00
G1 X10.82 Y12.93 F3500.00`

const gcodeList = gcode.split('\n').map(g => {
  const obj = gcodeToObject(g)
  if (!obj) return null
  const { x, y } = obj.args
  obj.args.x = Math.round(x * 10 - offset.gcode.x + offset.board.x)
  obj.args.y = Math.round(y * 10 - offset.gcode.y + offset.board.y)
  return obj
})

export const angleList = gcodeList.map(g => {
  const p2 = g.args
  const angle20x = getOxAngle(points[0], p2)
  const angle102 = getTriangleAngle(r1, getDistance(points[0], p2), r2)
  const angle10x = angle20x + angle102
  const angle24x = getOxAngle(points[4], p2)
  const angle342 = getTriangleAngle(r1, getDistance(points[4], p2), r2)
  const angle34x = angle24x - angle342
  return [angle10x, angle34x]
})

// const test = { x: 100, y: 220 }
// console.log('>> angle20x', getOxAngle(points[0], test))
// console.log('>> angle102', getTriangleAngle(r1, getDistance(points[0], test), r2))

export function getDistance(pA, pB) {
  let dx = pA.x - pB.x;
  let dy = pA.y - pB.y;
  return Math.sqrt(dx * dx + dy * dy);
}

export function getOxAngle(pA, pB) {
  const x1 = pA.x, y1 = pA.y;
  const x2 = pB.x, y2 = pB.y;

  const vectorAB = { x: x2 - x1, y: y2 - y1 };
  const vectorOX = { x: 1, y: 0 };
  // Tính tích vô hướng của hai vector
  const dotProduct = vectorAB.x * vectorOX.x + vectorAB.y * vectorOX.y;
  // Tính độ dài của các vector
  const magnitudeAB = Math.sqrt(vectorAB.x ** 2 + vectorAB.y ** 2);
  const magnitudeOX = Math.sqrt(vectorOX.x ** 2 + vectorOX.y ** 2);
  // Tính cos của góc
  const cosTheta = dotProduct / (magnitudeAB * magnitudeOX);

  const angleInRadians = Math.acos(cosTheta);
  const angleInDegrees = angleInRadians * (180 / Math.PI);
  return Math.round(angleInDegrees);
}
// console.log('>> 1:', getOxAngle(points[0], { x: 0, y: 100 }))
// console.log('>> 2:', getOxAngle(points[0], { x: 120, y: 100 }))

export function getTriangleAngle(a, b, c) {  // (side, side, opposite)
  if (a + b <= c || a + c <= b || b + c <= a) return;
  // const angleA = Math.acos((b * b + c * c - a * a) / (2 * b * c));
  // const angleB = Math.acos((a * a + c * c - b * b) / (2 * a * c));
  const angleC = Math.acos((a * a + b * b - c * c) / (2 * a * b));
  return Math.round(angleC * (180 / Math.PI));
}

export function getIntersection(circle1, circle2, r = r2) {
  const { x: x1, y: y1 } = circle1;
  const { x: x2, y: y2 } = circle2;
  // Khoảng cách giữa hai trung tâm
  let d = Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2));
  // Kiểm tra nếu không có giao điểm
  if (d > 2 * r || d < Math.abs(r - r)) return;

  let a = (Math.pow(r, 2) - Math.pow(r, 2) + Math.pow(d, 2)) / (2 * d);
  let h = Math.sqrt(Math.pow(r, 2) - Math.pow(a, 2));

  // Tọa độ giao điểm
  let x0 = x1 + a * (x2 - x1) / d;
  let y0 = y1 + a * (y2 - y1) / d;

  let i1 = {
    x: Math.round(x0 + h * (y2 - y1) / d),
    y: Math.round(y0 - h * (x2 - x1) / d),
  }
  let i2 = {
    x: Math.round(x0 - h * (y2 - y1) / d),
    y: Math.round(y0 + h * (x2 - x1) / d),
  }
  // return [i1, i2];
  return i1.y > i2.y ? i1 : i2;
}
