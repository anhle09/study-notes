import { useEffect, useRef, useState } from "react";
import { angleList, points, getDistance, getIntersection } from "./driver";

// draw size ~ 100 * 100 (r = 150)
// { x: 100, y: 120 } -> { x: 200, y: 220 }
const canvas = { width: 300, height: 300 }
const lineLength = 100;   // Chiều dài cố định của mỗi đoạn thẳng
let dragging = null;      // Biến lưu trữ trạng thái khi kéo
const dots = []

// Tìm điểm 2 tạo với Ox một góc theta
function getThetaPoint(p1 = {}, theta = 90) {
  theta = theta * Math.PI / 180;
  const distance = getDistance(points[0], points[1])
  const x = p1.x + distance * Math.cos(theta);
  const y = distance * Math.sin(theta);
  return { x: Math.round(x), y: Math.round(y) };
}

export default function DrawApp() {
  const canvasRef = useRef(null);
  const [inputVal, setInputVal] = useState([90, 90])
  let ctx = canvasRef.current?.getContext('2d');

  function drawText(text, position, config = {}) {
    const {
      color = 'gray',
      font = '14px Arial',
      align = 'center',
      baseLine = 'middle',
    } = config
    ctx.font = font;
    ctx.fillStyle = color;
    ctx.textAlign = align;
    ctx.textBaseline = baseLine;
    ctx.fillText(text, position.x, position.y);
  }

  function drawLine(p1, p2, color = 'gray') {
    ctx.strokeStyle = color;
    // ctx.lineWidth = 1;
    ctx.beginPath();
    ctx.moveTo(p1.x, p1.y);
    ctx.lineTo(p2.x, p2.y);
    ctx.stroke();
  }

  function drawCircle(position, radius) {
    ctx.beginPath();
    ctx.arc(position.x, position.y, radius, 0, 2 * Math.PI);
    ctx.fillStyle = 'lightgray';
    ctx.fill();
    ctx.stroke();
  }

  function drawGrid() {
    const gridSize = 20;
    // Draw vertical grid lines
    for (let x = gridSize; x < canvas.width; x += gridSize) {
      let start = { x, y: 0 }
      let end = { x, y: canvas.height }
      drawLine(start, end, "#e0e0e0")
      drawText(x / 10, start, { color: "#e0e0e0", baseLine: "top" })
    }
    // Draw horizontal grid lines
    for (let y = gridSize; y < canvas.height; y += gridSize) {
      let start = { x: 0, y }
      let end = { x: canvas.width, y }
      drawLine(start, end, "#e0e0e0")
      drawText(y / 10, start, { color: "#e0e0e0", align: "left" })
    }
  }

  // Vẽ các đoạn thẳng nối tiếp nhau và các điểm tròn với nền màu xám và chữ số
  function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height); // Xóa canvas
    drawGrid();
    // Vẽ các đoạn thẳng nối tiếp nhau
    for (let i = 0; i < points.length - 1; i++) {
      drawLine(points[i], points[i + 1])
    }
    points.forEach(point => {
      drawCircle(point, point.radius);
      drawText(point.label, point);
    })
    dots.forEach(dot => drawCircle(dot, 1));
  }

  // Kiểm tra xem điểm nào đang bị kéo
  function isMouseOverPoint(mouseX, mouseY, point) {
    const dx = mouseX - point.x;
    const dy = mouseY - point.y;
    return (dx * dx + dy * dy) <= (point.radius * point.radius);
  }

  const mousedown = (e) => {
    const mouseX = e.pageX;
    const mouseY = e.pageY;
    // Kiểm tra nếu người dùng nhấn vào một điểm tròn
    points.forEach((point, index) => {
      if (!isMouseOverPoint(mouseX, mouseY, point)) return;
      dragging = index; // Lưu chỉ số của điểm đang kéo
    })
  }

  function updatePoint(index, base) {
    const distance = getDistance(points[index], points[base]);
    const scale = lineLength / distance;
    const dx = points[index].x - points[base].x;
    const dy = points[index].y - points[base].y;
    points[index].x = Math.round(points[base].x + dx * scale);
    points[index].y = Math.round(points[base].y + dy * scale);
  }

  function updatePoints() {
    updatePoint(1, 0);
    updatePoint(3, 4);
    Object.assign(points[2], getIntersection(points[1], points[3]));
    dots.push({ ...points[2] });
    draw();
  }

  const mousemove = (e) => {
    if (dragging === null) return;
    const mouseX = e.pageX;
    const mouseY = e.pageY;
    points[dragging].x = mouseX;
    points[dragging].y = mouseY;
    updatePoints();
  }

  const mouseup = () => {
    dragging = null;
  }

  useEffect(() => {
    ctx = canvasRef.current?.getContext('2d');
    draw();
    // runTest();
    run();
  }, [])

  const changeInput = (val, key) => {
    // const val = +e.target.value;
    inputVal[key] = val;
    Object.assign(points[1], getThetaPoint(points[0], inputVal[0]));
    Object.assign(points[3], getThetaPoint(points[4], inputVal[1]));
    updatePoints();
    setInputVal([...inputVal]);
  }

  const runTest = async () => {
    for (let i = 80; i < 180; i += 5) {
      changeInput(i, 0)
      for (let j = 0; j < 100; j += 5) {
        changeInput(j, 1)
        await new Promise(res => setTimeout(() => res(), 1))
      }
    }
    // for (let i = 60; i < 180; i++) {
    //   changeInput(i, 0)
    //   for (let j = 0; j < 120; j++) {
    //     changeInput(j, 1)
    //     await new Promise(res => setTimeout(() => res(), 1))
    //   }
    // }
  }

  const run = async () => {
    console.log('>>', angleList)
    for (let i = 0; i < angleList.length; i++) {
      const [a1, a2] = angleList[i]
      Object.assign(points[1], getThetaPoint(points[0], a1))
      Object.assign(points[3], getThetaPoint(points[4], a2))
      updatePoints();
      setInputVal([a1, a2]);
      await new Promise(res => setTimeout(() => res(), 100))
    }
  }

  return (
    <div>
      <canvas
        ref={canvasRef}
        style={{ border: "1px solid lightgray" }}
        width={canvas.width}
        height={canvas.height}
        onMouseDown={mousedown}
        onMouseMove={mousemove}
        onMouseUp={mouseup}
      />
      <div>
        <span>Angle 1</span>
        <input
          value={inputVal[0]}
          onChange={e => changeInput(+e.target.value, 0)}
        />
      </div>
      <div>
        <span>Angle 2</span>
        <input
          value={inputVal[1]}
          onChange={e => changeInput(+e.target.value, 1)}
        />
      </div>
    </div>
  )
}
