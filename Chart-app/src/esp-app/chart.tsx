import { Line } from "react-chartjs-2"
import {
  Chart,
  LineElement,
  CategoryScale,
  LinearScale,
  PointElement,
  // Title,
  Tooltip,
  Legend
} from "chart.js"

Chart.register(
  LineElement,
  CategoryScale,
  LinearScale,
  PointElement,
  // Title,
  Tooltip,
  Legend
)

const colors = ["gray", "rgb(75, 192, 192)", "rgb(255, 99, 132)", "lightgreen"]

export function LineChart({ labels, data }: any) {
  const datasets: any = []
  for (let key in data) {
    datasets.push({
      data: data[key],
      label: key,
      pointStyle: false,
      borderColor: colors.shift(),
    })
  }
  const chartData: any = {
    labels,
    datasets,
    // datasets: [
    //   {
    //     data: data.servo,
    //     label: "servo",
    //     pointStyle: false,
    //     borderColor: "gray",
    //   }, {
    //     data: data.sensor,
    //     label: "Sensor",
    //     pointStyle: false,
    //     borderColor: "rgb(75, 192, 192)",
    //   }, {
    //     data: data.p,
    //     label: "P",
    //     pointStyle: false,
    //     borderColor: "rgb(255, 99, 132)",
    //   }, {
    //     data: data.i,
    //     label: "I",
    //     pointStyle: false,
    //     borderColor: "lightgreen",
    //     // fill: false,
    //     // tension: 0.1,
    //   },
    // ],
  }

  const chartOptions = {
    responsive: true,
    scales: {
      x: {
        title: {
          display: true,
          text: "Second",
        },
      },
      y: {
        title: {
          display: true,
          text: "Value",
        },
        // beginAtZero: true,
      },
    },
    animation: {
      duration: 600,
    },
  }

  return (
    <Line
      data={chartData}
      options={chartOptions}
    />
  )
}
