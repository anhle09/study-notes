import { useRef, useState } from 'react'
import { LineChart } from './chart'

export default function EspApp() {
  const [chartData, setChartData] = useState<any>({ labels: [], data: {} })
  const [isConnected, setIsConnected] = useState(false)
  const tmp = useRef<any>({})
  const { socket } = tmp.current

  const connect = () => {
    const ws = new WebSocket('ws://')
    ws.onopen = () => {
      console.log('Connected')
      setIsConnected(true)
    }
    ws.onmessage = (e) => {
      console.log('Message', e.data)
      const data = JSON.parse(e.data)
      setChartData((prev: any) => {
        prev.labels.push(prev.labels.length)
        for (let key in data) {
          if (prev.data[key]) prev.data[key].push(data.key)
          else prev.data[key] = [data.key]
        }
        return { ...prev }
      })
    }
    ws.onclose = () => console.log('Disconnected')
    ws.onerror = (error) => console.error('Error:', error)
    tmp.current.socket = ws
  }

  const disconnect = () => {
    socket && socket.close()
    setIsConnected(false)
  }

  return (
    <div>
      <button onClick={isConnected ? disconnect : connect}>
        {isConnected ? 'Connected' : 'Connect'}
      </button>
      <LineChart {...chartData} />
    </div>
  )
}
