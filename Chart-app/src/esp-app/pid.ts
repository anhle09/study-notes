export class PID {
  maxOutput = 30
  minOutput = 0
  integral = 0
  lastError = 0

  calculate({ target, current, kp, ki = 0, kd = 0 }: any) {
    const error = target - current
    let p = Math.round(kp * error)

    this.integral += error
    const i = Math.round(ki * this.integral)

    const derivative = error - this.lastError
    const d = Math.round(kd * derivative)
    this.lastError = error

    let res = p + i + d
    if (res > this.maxOutput) res = this.maxOutput
    if (res < this.minOutput) res = this.minOutput
    return { res, p, i, d }
  }
}
