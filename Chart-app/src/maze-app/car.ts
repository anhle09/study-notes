import { maze, cellSize } from './maze'

const carStep = 1
const carSize = cellSize
const sensorOffset = cellSize + carStep
const initPosition = cellSize * 2
const maxCount = { right: 12, left: 60 }

class Car {
  position = { x: initPosition, y: initPosition }
  direction = 'down'
  intervalId = null as any
  count = { right: 0, left: 0 }

  mapDirection = (upCb: any, downCb: any, rightCb: any, leftCb: any) => {
    switch (this.direction) {
      case 'up': return upCb()
      case 'down': return downCb()
      case 'right': return rightCb()
      case 'left': return leftCb()
      default: return null
    }
  }

  goUp = () => this.position.y = this.position.y - carStep
  goDown = () => this.position.y = this.position.y + carStep
  goRight = () => this.position.x = this.position.x + carStep
  goLeft = () => this.position.x = this.position.x - carStep

  forward = () => {
    this.mapDirection(this.goUp, this.goDown, this.goRight, this.goLeft)
    /* add a little left */
    if (++this.count.left < maxCount.left) return
    this.count.left = 0
    this.rotateLeft()
    this.rotateRight()
  }
  rotateRight = () => {
    this.direction = this.mapDirection(() => 'right', () => 'left', () => 'down', () => 'up')
    this.forward()
  }
  rotateLeft = () => {
    this.direction = this.mapDirection(() => 'left', () => 'right', () => 'up', () => 'down')
    this.forward()
  }
  reverse = () => {
    this.direction = this.mapDirection(() => 'down', () => 'up', () => 'left', () => 'right')
  }
  turnRight = () => {
    this.rotateRight()
    if (++this.count.right < maxCount.right) this.rotateLeft()
    else this.count.right = 0
  }

  goTest = () => {
    if (this.sensorDown()) this.direction = 'up'
    if (this.sensorUp()) this.direction = 'down'
    if (this.direction === 'up') this.goUp()
    else this.goDown()
  }

  followRightWall = () => {
    const [left, front, right] = this.getSensor()
    if (!right) return this.turnRight()
    if (!front) return this.forward()
    if (!left) return this.rotateLeft()
    if (front) return this.reverse()
  }

  start = () => this.intervalId = setInterval(this.followRightWall, 10)
  stop = () => this.intervalId && clearInterval(this.intervalId)

  detectedUp = (isWall = true) => {
    const step = isWall ? carStep : sensorOffset
    const nextY = Math.floor((this.position.y - step) / cellSize)
    const nextX = Math.floor(this.position.x / cellSize)
    return maze[nextY][nextX] || maze[nextY][nextX + 1]
  }

  detectedDown = (isWall = true) => {
    const step = isWall ? carStep : sensorOffset
    const nextY = Math.floor((this.position.y + carSize + step) / cellSize)
    const nextX = Math.floor(this.position.x / cellSize)
    return maze[nextY][nextX] || maze[nextY][nextX + 1]
  }

  detectedRight = (isWall = true) => {
    const step = isWall ? carStep : sensorOffset
    const nextY = Math.floor(this.position.y / cellSize)
    const nextX = Math.floor((this.position.x + carSize + step) / cellSize)
    return maze[nextY][nextX] || maze[nextY + 1][nextX]
  }

  detectedLeft = (isWall = true) => {
    const step = isWall ? carStep : sensorOffset
    const nextY = Math.floor(this.position.y / cellSize)
    const nextX = Math.floor((this.position.x - step) / cellSize)
    return maze[nextY][nextX] || maze[nextY + 1][nextX]
  }

  sensorUp = () => this.detectedUp() || this.detectedUp(false)
  sensorDown = () => this.detectedDown() || this.detectedDown(false)
  sensorRight = () => this.detectedRight() || this.detectedRight(false)
  sensorLeft = () => this.detectedLeft() || this.detectedLeft(false)

  getSensor = () => {
    const { sensorUp, sensorDown, sensorRight, sensorLeft } = this
    return this.mapDirection(
      () => [sensorLeft(), sensorUp(), sensorRight()],
      () => [sensorRight(), sensorDown(), sensorLeft()],
      () => [sensorUp(), sensorRight(), sensorDown()],
      () => [sensorDown(), sensorLeft(), sensorUp()],
    )
  }

  handleKeyDown = (e: any) => {
    switch (e.key) {
      case 'ArrowUp':
        // this.direction = 'up'
        if (!this.detectedUp()) this.goUp()
        break
      case 'ArrowDown':
        // this.direction = 'down'
        if (!this.detectedDown()) this.goDown()
        break
      case 'ArrowRight':
        // this.direction = 'right'
        if (!this.detectedRight()) this.goRight()
        break
      case 'ArrowLeft':
        // this.direction = 'left'
        if (!this.detectedLeft()) this.goLeft()
        break
      default:
        break
    }
  }
  
  draw = (ctx: any) => {
    const borderHeight = 3
    const carHeight = carSize - borderHeight
    const { x, y } = this.position
    if (this.direction === 'down') {
      ctx.fillStyle = 'gray'
      ctx.fillRect(x, y, carSize, carHeight)
      ctx.fillStyle = 'orange'
      ctx.fillRect(x, y + carHeight, carSize, borderHeight)
    }
    else if (this.direction === 'up') {
      ctx.fillStyle = 'orange'
      ctx.fillRect(x, y, carSize, borderHeight)
      ctx.fillStyle = 'gray'
      ctx.fillRect(x, y + borderHeight, carSize, carHeight)
    }
    else if (this.direction === 'right') {
      ctx.fillStyle = 'gray'
      ctx.fillRect(x, y, carHeight, carSize)
      ctx.fillStyle = 'orange'
      ctx.fillRect(x + carHeight, y, borderHeight, carSize)
    }
    else if (this.direction === 'left') {
      ctx.fillStyle = 'orange'
      ctx.fillRect(x, y, borderHeight, carSize)
      ctx.fillStyle = 'gray'
      ctx.fillRect(x + borderHeight, y, carHeight, carSize)
    }
  }

}

export const car = new Car()
