import { useRef, useEffect, useState } from 'react'
import { mazeWidth, mazeHeight, drawMaze } from './maze'
import { car } from './car'

export default function MazeCar() {
  const canvasRef = useRef<any>(null)
  const [started, setStarted] = useState(false)

  useEffect(() => {
    const ctx = canvasRef.current.getContext('2d')
    const intervalId = setInterval(() => {
      ctx.clearRect(0, 0, mazeWidth, mazeHeight)
      drawMaze(ctx)
      car.draw(ctx)
    }, 1000 / 60) // 60 FPS

    document.body.addEventListener('keydown', car.handleKeyDown)

    return () => {
      clearInterval(intervalId)
      document.body.removeEventListener('keydown', car.handleKeyDown)
    }
  }, [])

  const toggle = () => {
    started ? car.stop() : car.start()
    setStarted(prev => !prev)
  }

  return (
    <div style={{ padding: 16 }}>
      <p>
        <button onClick={toggle}>{ started ? 'stop' : 'start' }</button>
      </p>
      <canvas
        ref={canvasRef}
        width={mazeWidth}
        height={mazeHeight}
      />
    </div>
  )
}
