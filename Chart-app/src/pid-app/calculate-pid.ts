import { round } from "./motor"

// const kp = 1.0, ki = 0.1, kd = 0.01
const maxPwm = 255
let integral = 0, lastError = 0

export function calculatePid({ target, current, kp, ki, kd = 0 }: any) {
    const error = target - current
    let p = round(kp * error)
    if (p > maxPwm) p = maxPwm

    integral += error
    const i = round(ki * integral)

    const derivative = error - lastError
    const d = round(kd * derivative)
    lastError = error
    
    let res = p + i + d
    if (res > maxPwm) res = maxPwm
    if (res < 0) res = 0
    return { result: round(res, 0), p, i, d }
}

export class PID {
    maxOutput = 255
    integral = 0
    lastError = 0

    calculate({ target, current, kp, ki, kd = 0 }: any) {
        const error = target - current
        let p = round(kp * error)
        if (p > this.maxOutput) p = this.maxOutput
    
        this.integral += error
        const i = round(ki * this.integral)
    
        const derivative = error - this.lastError
        const d = round(kd * derivative)
        this.lastError = error
        
        let res = p + i + d
        if (res > this.maxOutput) res = this.maxOutput
        if (res < 0) res = 0
        return { result: round(res, 0), p, i, d }
    }
}
