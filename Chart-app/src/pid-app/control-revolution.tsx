import { useRef, useState } from "react"
import { Line } from "react-chartjs-2"
import {
    Chart,
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    // Title,
    Tooltip,
    Legend
} from "chart.js"

import { motor, round } from "./motor"
import { calculatePid } from './calculate-pid'

Chart.register(
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    // Title,
    Tooltip,
    Legend
)

const chartOptions = {
    responsive: true,
    scales: {
        x: {
            title: {
                display: true,
                text: "Second",
            },
        },
        y: {
            title: {
                display: true,
                text: "Revolution",
            },
            // beginAtZero: true,
        },
    },
    animation: {
        duration: 600,
    },
}

export default function ControlRevolution() {
    const initData = {
        times: [],
        speeds: [],
        revolutions: [],
        sensorVals: [],
    }
    const [motorData, setMotorData] = useState(initData as any)
    const [pwmList, setPwmList] = useState([0])
    const [target, setTarget] = useState(500)
    const tmp = useRef({} as any)

    const readSensor = () => {
        const { time, speed, revolution } = motor.state
        const speedReducer = 10     // he so giam toc (lan)
        const res = 1000             // bien tro (om)
        const sensorVal = round(revolution / speedReducer * res)
        setMotorData((prev: any) => ({
            times: [...prev.times, time],
            speeds: [...prev.speeds, speed],
            revolutions: [...prev.revolutions, revolution],
            sensorVals: [...prev.sensorVals, round(sensorVal / 10)]
        }))
        // console.log({ time, speed, revolution, sensorVal })
        return sensorVal
    }

    const setMotorPwm = (pwm = 0) => {
        if (!tmp.current.isStart) startLoop()
        tmp.current.pwm = pwm
        tmp.current.isStart = true
    }

    const loop = (interval: number) => {
        if (!tmp.current.isStart) return
        const current = readSensor()

        // const offsetPwm255 = round(target * 0.5)
        // if (current > target - offsetPwm255) tmp.current.pwm = 0
        // setPwmList((prev: any) => [...prev, tmp.current.pwm])
        // motor.setPwm(tmp.current.pwm, interval)

        const { result: pwm } = calculatePid({ target, current, kp: 0.05, kd: 0 })
        setPwmList((prev: any) => [...prev, round(pwm / 10)])
        motor.setPwm(pwm, interval)
    }

    const startLoop = () => {
        const interval = 50
        tmp.current.intervalId = setInterval(() => loop(interval), interval) as any
    }

    const stopLoop = () => {
        const { intervalId } = tmp.current
        if (!intervalId) return
        clearInterval(intervalId)
        tmp.current.isStart = false
    }

    const chartData: any = {
        labels: motorData.times,
        datasets: [
            {
                data: motorData.speeds,
                label: "Speed",
                pointStyle: false,
                borderColor: "gray",
                // fill: false,
                // tension: 0.1,
            }, {
                data: motorData.revolutions,
                label: "Revolution",
                pointStyle: false,
                borderColor: "rgb(75, 192, 192)",
            }, {
                data: motorData.sensorVals,
                label: "Sensor value",
                pointStyle: false,
                borderColor: "rgb(255, 99, 132)",
            }, {
                data: pwmList,
                label: "Pwm",
                pointStyle: false,
            },
        ],
    }

    return (
        <div>
            <h2>Chart</h2>
            {/* <button onClick={motor.forward}>Forward</button> */}
            {/* <button onClick={motor.backward}>Backward</button> */}
            <button onClick={stopLoop}>Stop</button>
            <button onClick={e => setMotorPwm(0)}>pwm = 0</button>
            <button onClick={e => setMotorPwm(50)}>pwm = 50</button>
            <button onClick={e => setMotorPwm(100)}>pwm = 100</button>
            <button onClick={e => setMotorPwm(255)}>pwm = 255</button>
            <Line
                data={chartData}
                options={chartOptions}
            />
            <div style={{ margin: 20, display: "flex" }}>
                <span>{ target }</span>
                <input
                    style={{ width: 400, marginLeft: 10 }}
                    type="range"
                    min="1"
                    max="1000"
                    value={target}
                    onChange={e => setTarget(+e.target.value)}
                    // defaultValue="500"
                    // onBlur={e => setTarget(+e.target.value)}
                />
            </div>
        </div>
    )
}
