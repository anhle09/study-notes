import { useEffect, useRef, useState } from "react"
import { Line } from "react-chartjs-2"
import {
    Chart,
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    // Title,
    Tooltip,
    Legend
} from "chart.js"

import { Motor, round } from "./motor"
import { PID } from './calculate-pid'

Chart.register(
    LineElement,
    CategoryScale,
    LinearScale,
    PointElement,
    // Title,
    Tooltip,
    Legend
)

function SpeedChart({ target = 0 }) {
    const initData = {
        times: [],
        speeds: [],
    }
    const initPid = {
        pwm: [0],
        p: [0],
        i: [0],
        d: [0],
    }
    const [motorData, setMotorData] = useState<any>(initData)
    const [pidResult, setPidResult] = useState<any>(initPid)
    // const [target, setTarget] = useState(50)
    const tmp = useRef<any>({})

    useEffect(() => {
        tmp.current.pid = new PID()
        tmp.current.motor = new Motor()
    }, [])

    const readSensor = () => {
        const { time, speed } = tmp.current.motor.state
        setMotorData((prev: any) => ({
            times: [...prev.times, time],
            speeds: [...prev.speeds, speed],
        }))
        return speed
    }

    const setMotorPwm = (pwm = 0) => {
        startLoop()
        tmp.current.pwm = pwm
    }

    const loop = (interval: number) => {
        const { pid } = tmp.current
        const current = readSensor()
        // const { result: pwm, p, i, d } = pid.calculate({ target, current, kp: 15, ki: 0, kd: 0 })     // dao dong lon
        // const { result: pwm, p, i, d } = pid.calculate({ target, current, kp: 10, ki: 0.3, kd: 0 })   // do vot lo lon
        // const { result: pwm, p, i, d } = pid.calculate({ target, current, kp: 12, ki: 0.25, kd: 0 })  // < target
        // const { result: pwm, p, i, d } = pid.calculate({ target, current, kp: 13, ki: 0.28, kd: 0 })  // sai so lon
        const { result: pwm, p, i, d } = pid.calculate({ target, current, kp: 13, ki: 0.28, kd: 0.1 })   // ko khac may

        setPidResult((prev: any) => ({
            pwm: [...prev.pwm, pwm / 10],
            p: [...prev.p, round(p / 10)],
            i: [...prev.i, round(i / 10)],
            d: [...prev.d, d],
        }))
        tmp.current.motor.setPwm(pwm, interval)
    }

    const startLoop = () => {
        const interval = 50
        tmp.current.intervalId = setInterval(() => loop(interval), interval) as any
    }

    const stopLoop = () => {
        const { intervalId } = tmp.current
        if (!intervalId) return
        clearInterval(intervalId)
    }

    const chartData: any = {
        labels: motorData.times,
        datasets: [
            {
                data: motorData.speeds,
                label: "Speed",
                pointStyle: false,
                borderColor: "gray",
                // fill: false,
                // tension: 0.1,
            }, {
                data: pidResult.p,
                label: "P",
                pointStyle: false,
                borderColor: "rgb(75, 192, 192)",
            }, {
                data: pidResult.i,
                label: "I",
                pointStyle: false,
                borderColor: "rgb(255, 99, 132)",
            }, {
                data: pidResult.d,
                label: "D",
                pointStyle: false,
                borderColor: "lightgreen",
            }, {
                data: pidResult.pwm,
                label: "Pwm",
                pointStyle: false,
            },
        ],
    }

    const chartOptions = {
        responsive: true,
        scales: {
            x: {
                title: {
                    display: true,
                    text: "Second",
                },
            },
            y: {
                title: {
                    display: true,
                    text: "Value",
                },
                // beginAtZero: true,
            },
        },
        animation: {
            duration: 600,
        },
    }
    
    return (
        <div>
            <button onClick={stopLoop}>Stop</button>
            <button onClick={startLoop}>Start</button>
            <button onClick={e => setMotorPwm(96)}>pwm = 96</button>
            <b style={{ paddingLeft: 10 }}>Target: {target}</b>
            <Line
                data={chartData}
                options={chartOptions}
            />
            {/* <div style={{ margin: 20, display: "flex" }}>
                <span style={{ width: 80 }}>
                    Target: { target }
                </span>
                <input
                    style={{ width: 400, marginLeft: 10 }}
                    type="range"
                    min="0"
                    max="133"
                    value={target}
                    onChange={e => setTarget(+e.target.value)}
                />
            </div>
            <div style={{ margin: 20, display: "flex" }}>
                <span style={{ width: 80 }}>
                    Pwm: { tmp.current.pwm }
                </span>
                <input
                    style={{ width: 400, marginLeft: 10 }}
                    type="range"
                    min="0"
                    max="255"
                    // value={target}
                    onChange={e => tmp.current.pwm = +e.target.value}
                />
            </div> */}
        </div>
    )
}

export default function ControlSpeed() {
    const targets = [30, 50, 100]
    return (
        <div>
            {targets.map(t => <SpeedChart key={t} target={t} />)}
        </div>
    )
}
