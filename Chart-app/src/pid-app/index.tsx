import ControlRevolution from "./control-revolution"
import ControlSpeed from "./control-speed"

export default function PidApp() {
    const page: string = "speed"
    switch (page) {
        case "revo": return <ControlRevolution />
        case "speed": return <ControlSpeed />
        default: return null
    }
}
