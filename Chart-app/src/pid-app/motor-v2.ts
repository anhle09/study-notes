const maxSpeed = 8000                   // rpm
const startTime = 2000, interval = 50   // ms
const minPwm = 50, maxPwm = 255
const step = maxSpeed / startTime
// const forceStep = step * 3

/** case
 * tang pwm
 * giam pwm
 * reverse (forceStop)
 */

/** setPwm(100) // tang
 * moi step se tang speed toi max se ko tang nua
 * speed += step
 * max = pwm / maxPwm * maxSpeed
 */

/** setPwm(50)  // giam
 * moi step se giam speed toi max se ko giam nua
 * speed -= step
 * max = pwm / maxPwm * maxSpeed
 */

/** forceStop()
 * moi step se tang/giam ve 0
 * speed +-= forceStep
 */

export function round(val: number, place = 2) {
    const num = 10 ** place
    return Math.round(val * num) / num
}

export const motor = {
    id: undefined as any,
    pwm: 0,     // 1 - 255
    time: 0,    // milisecond
    speed: 0,   // 0 - 10,000
    revolution: 0,
    isReverse: false,
    // isForceStop: false,
    forward() {
        // if (this.speed) this.forceStop()
        this.isReverse = false
    },
    // backward() {
    //     if (this.speed) this.forceStop()
    //     this.isReverse = true
    // },
    // forceStop() {
    //     this.isForceStop = true
    // },
    setPwm(pwm = 0, intervalFn: any) {
        if (this.id) clearInterval(this.id)
        const isDesc = pwm <= this.pwm
        this.pwm = pwm
        let ratio = 1, target = 0
        if (pwm >= minPwm) {
            ratio = round(pwm / maxPwm)
            target = round(ratio * maxSpeed)
        }
        let speedStep = round(ratio * step * interval)
        if (isDesc) speedStep = -speedStep

        const handleInterval = () => {
            this.time += interval
            const limit = target - speedStep
            if (isDesc ? this.speed >= limit : this.speed <= limit) {
                this.speed += speedStep
            }
            else {
                this.speed = target
            }

            this.revolution += round(this.speed / (60 * 1000) * interval)
            const speedPerSecond = round(this.speed / 60)
            const state = {
                time: this.time / 1000,     // second                                      
                speed: this.isReverse ? -speedPerSecond : speedPerSecond,
                revolution: round(this.revolution),
            }
            intervalFn(state)
        }
        this.id = setInterval(handleInterval, interval)
    },
    clear() {
        if (this.id) clearInterval(this.id)
    },
}
