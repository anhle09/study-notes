const maxSpeed = 8000       // rpm
const startTime = 1000      // ms
const maxPwm = 255
const step = maxSpeed / startTime
// const forceStep = step * 3

/** case
 * tang pwm
 * giam pwm
 * reverse (forceStop)
 */

/** setPwm(100) // tang
 * moi step se tang speed toi max se ko tang nua
 * speed += step
 * max = pwm / maxPwm * maxSpeed
 */

/** setPwm(50)  // giam
 * moi step se giam speed toi max se ko giam nua
 * speed -= step
 * max = pwm / maxPwm * maxSpeed
 */

export function round(val: number, place = 2) {
    const num = 10 ** place
    return Math.round(val * num) / num
}

export const motor = {
    id: undefined as any,
    pwm: 0,     // 1 - 255
    time: 0,    // milisecond
    speed: 0,   // 0 - 10,000
    revolution: 0,
    state: { time: 0, speed: 0, revolution: 0 },
    isDesc: false,
    setPwm(pwm = 0, interval = 100) {
        let ratio = 1, target = 0
        if (pwm > 0) {
            ratio = round(pwm / maxPwm)
            target = round(ratio * maxSpeed)
        }
        let speedStep = round(ratio * step * interval)

        if (pwm < this.pwm && this.speed > target) this.isDesc = true
        if (pwm > this.pwm) this.isDesc = false
        this.pwm = pwm
        if (this.isDesc) speedStep = -speedStep

        const limit = target - speedStep
        if (this.isDesc ? this.speed >= limit : this.speed <= limit) {
            this.speed += speedStep
        }
        else {
            this.speed = target
        }

        this.time += interval
        this.revolution += round(this.speed / (60 * 1000) * interval)
        const speedPerSecond = round(this.speed / 60)
        this.state = {
            time: this.time / 1000,     // second                                      
            speed: speedPerSecond,
            revolution: round(this.revolution),
        }
    },
}

export class Motor {
    id = undefined as any
    pwm = 0     // 1 - 255
    time = 0    // milisecond
    speed = 0   // 0 - 10,000
    revolution = 0
    state = { time: 0, speed: 0, revolution: 0 }
    isDesc = false

    setPwm(pwm = 0, interval = 100) {
        let ratio = 1, target = 0
        if (pwm > 0) {
            ratio = round(pwm / maxPwm)
            target = round(ratio * maxSpeed)
        }
        let speedStep = round(ratio * step * interval)

        if (pwm < this.pwm && this.speed > target) this.isDesc = true
        if (pwm > this.pwm) this.isDesc = false
        this.pwm = pwm
        if (this.isDesc) speedStep = -speedStep

        const limit = target - speedStep
        if (this.isDesc ? this.speed >= limit : this.speed <= limit) {
            this.speed += speedStep
        }
        else {
            this.speed = target
        }
        this.time += interval
        this.revolution += round(this.speed / (60 * 1000) * interval)
        const speedPerSecond = round(this.speed / 60)
        this.state = {
            time: this.time / 1000,     // second                                      
            speed: speedPerSecond,
            revolution: round(this.revolution),
        }
    }
}
