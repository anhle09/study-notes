import { DateTime } from 'luxon'
import { ICandle, ICandleList } from './candle'
import { getUsUnixTime } from './time'

interface IApiCandle {
    t: ICandle
    l: ICandle
    o: ICandle
    c: ICandle
    h: ICandle
    v: ICandle
    s: string
}

interface IOption {
    symbol: string
    resolution: string
    from: number
    to: number
}

async function fetchData({ symbol, resolution, from, to }: IOption) {
    const url = `https://finnhub.io/api/v1/stock/candle?symbol=${symbol}&resolution=${resolution}&from=${from}&to=${to}&token=c8e4s8aad3i91d97edkg`
    const res = await fetch(url)
    const json = await res.json() as IApiCandle
    if (json.s !== 'ok') throw new Error('No data')
    return mapCandleData(json)
}

export function mapCandleData({ t, l, o, c, h }: IApiCandle) {
    const chartData = t.map((item, i) => {
        const time = DateTime.fromSeconds(item, { zone: 'America/New_York' })
        const label = time.minute ? time.minute : time.hour
        return [ String(label), l[i], o[i], c[i], h[i] ]
    })
    return chartData as ICandleList
}

export async function get5mData(symbol: string, date: string) {
    const [year, month, day] = date.split('-') // format yyyy-mm-dd
    const usTime = getUsUnixTime(Number(year), Number(month), Number(day), 9, 30)
    const option: IOption = {
        symbol,
        resolution: '5',
        from: usTime,
        to: usTime + (25 * 60), // 25m * 60s -> 10h
    }
    return await fetchData(option)
}

export async function get1hData(symbol: string, date: string) {
    const [year, month, day] = date.split('-') // format yyyy-mm-dd
    const usTime = getUsUnixTime(Number(year), Number(month), Number(day), 10, 0)
    const option: IOption = {
        symbol,
        resolution: '60',
        from: usTime,
        to: usTime + (5 * 60 * 60), // 5h * 60m * 60s -> 15h
    }
    return await fetchData(option)
}
