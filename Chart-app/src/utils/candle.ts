export enum INDEX { TIME, LOW, OPEN, CLOSE, HIGH }
export type ICandle = number[]
export type ICandleList = ICandle[]

const { OPEN, CLOSE } = INDEX

function isBullishCandle(candle: ICandle) {
    const percent = (candle[CLOSE] - candle[OPEN]) / candle[OPEN] * 100
    if (percent > -0.1) return true
    return false
}

function isBearishCandle(candle: ICandle) {
    const percent = (candle[CLOSE] - candle[OPEN]) / candle[OPEN] * 100
    if (percent < 0.1) return true
    return false
}

export function calculatePercent(open: number, close: number) {
    const percent = (close - open) / open * 100
    return Math.round(percent * 10) / 10    // rounding numbers to 1 digits after comma
}

export function getTotalPercent(candleList: ICandleList) {
    const firstCandle = candleList[0]
    const lastCandle = candleList[candleList.length - 1]
    return calculatePercent(firstCandle[OPEN], lastCandle[CLOSE])
}

export function getCandleSummary(candleList: ICandleList) {
    let chain = 0, priority = 5    // maximum
    const percent = getTotalPercent(candleList)

    if (percent > 0.5) {
        for (let i = 0; i < candleList.length; i++) {
            if (isBullishCandle(candleList[i])) {
                chain = 0
            } else {
                chain++
                priority--
            }
            if (chain >= 2) {
                priority = 0
                break
            }
            if (i >= 5) break
        }
    }
    else if (percent < -0.5) {
        for (let i = 0; i < candleList.length; i++) {
            if (isBearishCandle(candleList[i])) {
                chain = 0
            } else {
                chain++
                priority--
            }
            if (chain >= 2) {
                priority = 0
                break
            }
            if (i >= 5) break
        }
    }
    else {
        priority = 0
    }
    return { percent, priority }
}
