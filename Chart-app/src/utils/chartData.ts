import { DateTime } from 'luxon'
import { get1hData, get5mData } from './api'
import { calculatePercent, getCandleSummary, ICandle, ICandleList, INDEX } from './candle'
import { getSymbolTrend, TREND } from './symbolTrend'

export type IChartData = (string | number)[][]

export interface IChartSummary {
    date: string
    symbol: string
    chartData: IChartData
    percent: number
    priority: number
    profit?: number[]
    trend?: TREND
}

const chartHead = ["time", "low", "open", "close", "high"]
const { OPEN, CLOSE } = INDEX

function getChartProfit(chartData: IChartData) {
    const firstCandle = chartData[7] as ICandle
    const middleCandle = chartData[9] as ICandle | undefined
    const lastCandle = chartData[12] as ICandle | undefined
    const profit = []
    if (middleCandle) {
        profit.push(calculatePercent(firstCandle[OPEN], middleCandle[CLOSE]))
    }
    if (lastCandle) {
        profit.push(calculatePercent(firstCandle[OPEN], lastCandle[CLOSE]))
    }
    return profit
}

function chartCompareFn(a: IChartSummary, b: IChartSummary) {
    if (b.priority > a.priority) return 1
    if (b.priority < a.priority) return -1
    if (b.percent > a.percent) return 1
    if (b.percent < a.percent) return -1
    return 0
}

interface IChartListOption {
    date: string
    symbolList: string[]
    expand1h: boolean
    sorted: boolean
}

export async function getChartList(option: IChartListOption) {
    const { date, symbolList, expand1h, sorted } = option
    const chart5mList = await Promise.all(symbolList.map(symbol => get5mData(symbol, date)))

    const summaryList = symbolList.map((symbol, index) => {
        const chartData = chart5mList[index]
        const summary = getCandleSummary(chartData)
        return { symbol, chartData, ...summary } as IChartSummary
    })
    if (sorted) summaryList.sort(chartCompareFn)
    const sortedSymbol = summaryList.map(item => item.symbol)

    let chart1hList: ICandleList[]
    if (expand1h) {
        chart1hList = await Promise.all(sortedSymbol.map(symbol => get1hData(symbol, date)))
    }

    const chartList = summaryList.map((summary, index) => {
        summary.chartData.unshift(chartHead as any)
        const symbolTrend = getSymbolTrend(date)
        if (symbolTrend) {
            summary.trend = symbolTrend[summary.symbol]
        }
        if (chart1hList) {
            summary.chartData.push(...chart1hList[index])
            summary.profit = getChartProfit(summary.chartData)
        }
        return summary
    })
    return chartList
}

interface IHistoryListOption {
    date: string
    symbol: string
    expand1h: boolean
    sorted: boolean
}

export async function getHistoryList(option: IHistoryListOption) {
    const { date, symbol, expand1h, sorted } = option
    const [year, month, day] = date.split('-')
    const symbolList = []
    for (let i = 1; i <= Number(day); i++) {
        const dateTime = DateTime.fromObject({ year: Number(year), month: Number(month), day: i })
        const weekday = dateTime.weekdayLong
        if (weekday !== 'Saturday' && weekday !== 'Sunday') {
            symbolList.push({ symbol, date: dateTime.toISODate() })
        }
    }
    const chart5mList = await Promise.all(symbolList.map(item => get5mData(item.symbol, item.date)))

    const summaryList = symbolList.map((item, index) => {
        const chartData = chart5mList[index]
        const summary = getCandleSummary(chartData)
        return { ...item, chartData, ...summary } as IChartSummary
    })
    if (sorted) summaryList.sort(chartCompareFn)
    const sortedSymbol = summaryList.map(item => ({
        symbol: item.symbol,
        date: item.date
    }))

    let chart1hList: ICandleList[]
    if (expand1h) {
        chart1hList = await Promise.all(sortedSymbol.map(item => get1hData(item.symbol, item.date)))
    }

    const chartList = summaryList.map((summary, index) => {
        summary.chartData.unshift(chartHead as any)
        if (chart1hList) {
            summary.chartData.push(...chart1hList[index])
            summary.profit = getChartProfit(summary.chartData)
        }
        return summary
    })
    return chartList
}
