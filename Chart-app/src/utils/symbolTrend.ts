import { getPreDate } from './time'

export enum TREND {
    UP = 'up',
    DW = 'down',
    SI = 'sideway',
    RE = 'resistance'
}
const { UP, DW, SI, RE } = TREND

type ITrend = { [key: string]: TREND }
type IDayTrend = { [key: number]: ITrend }
type IMonthTrend = { [key: number]: IDayTrend }

const data2022: IMonthTrend = {
    11: {
        11: { AAPL: UP, MSFT: RE, GOOG: RE, ORCL: RE, TSLA: UP, NVDA: UP, AMD: UP, HPQ: UP, DELL: UP, AMZN: UP, SHOP: UP, ABNB: UP, TSM: UP, OXY: UP, DVN: UP, FTCH: UP, NIO: UP, UPST: UP, META: UP, NFLX: UP },
        14: { AAPL: SI, MSFT: SI, GOOG: SI, ORCL: RE, TSLA: SI, NVDA: UP, AMD: SI, HPQ: UP, DELL: SI, AMZN: SI, SHOP: SI, ABNB: DW, TSM: SI, OXY: SI, DVN: SI, FTCH: SI, NIO: SI, UPST: DW, META: UP, NFLX: UP },
    }
}

export function getSymbolTrend(date: string) {
    const preDate = getPreDate(date)
    if (!preDate) return

    const monthData = data2022[preDate.month]
    if (!monthData) return

    return monthData[preDate.day]
}

export function getRemovedSymbol(date: string) {
    return JSON.parse(localStorage.getItem(date) as string) as string[] | null
}

export function getSymbolList(date: string, allSymbol = false) {
    const symbols = getSymbolTrend(date)
    if (!symbols) return

    const symbolList = Object.keys(symbols)
    const removedSymbol = getRemovedSymbol(date)
    if (allSymbol || !removedSymbol) return symbolList
    return symbolList.filter(item => !removedSymbol.includes(item))
}

export function storeRemovedSymbol(symbol: string, date: string) {
    let removedSymbol = getRemovedSymbol(date)
    if (removedSymbol) {
        removedSymbol.push(symbol)
    } else {
        removedSymbol = [symbol]
    }
    localStorage.setItem(date, JSON.stringify(removedSymbol))
}
