import { DateTime } from 'luxon'

export function getUsUnixTime(year: number, month: number, day: number, hour: number, minute: number) {
    // time.toLocaleString(DateTime.DATETIME_MED)
    const time = DateTime.fromObject({ year, month, day, hour, minute }, { zone: 'America/New_York' })
    return time.toUnixInteger()
}

export function getUsTimeNow() {
    return DateTime.now().setZone('America/New_York')
}

export function getPreDate(date: string) {
    const dateTime = DateTime.fromISO(date, { zone: 'America/New_York' })
    if (!dateTime.isValid) return
    
    if (dateTime.weekdayLong === 'Monday') {
        return dateTime.minus({ days: 3 })
    }
    else {
        return dateTime.minus({ days: 1 })
    }
}
