import { get5mData } from './api'
import { getCandleSummary, getTotalPercent } from './candle'
import { getPreDate, getUsTimeNow, getUsUnixTime } from './time'
import { getSampleData } from './sampleData'
import { getSymbolTrend } from './symbolTrend'

describe('candle.ts', () => {
    test('getTotalPercent', () => {
        const chart = [
            ['a', 1, 2, 2.05, 3],
            ['b', 1, 2.05, 2.1022, 3]
        ]
        const p = getTotalPercent(chart as any)
        expect(p).toEqual(5.1)
    })

    test('getCandleSummary = 5', () => {
        const chart = [
            ['a', 1, 2, 2.05, 3],
            ['b', 1, 2.05, 2.1022, 3]
        ]
        const summary = getCandleSummary(chart as any)
        expect(summary.priority).toEqual(5)
    })

    test('getCandleSummary = 4', () => {
        const chart = [
            ['a', 1, 2, 1.9, 3],
            ['b', 1, 1.9, 2.1, 3]
        ]
        const summary = getCandleSummary(chart as any)
        expect(summary.priority).toEqual(4)
    })
})

describe('time.ts', () => {
    test('getUsUnixTime', () => {
        const usTime = getUsUnixTime(2022, 11, 13, 12, 0)
        expect(usTime).toEqual(1668358800)
    })

    test('getUsTimeNow', () => {
        const usNow = getUsTimeNow()
        expect(usNow.second).toBeGreaterThan(0)
    })
    
    test('getPreDate', () => {
        const date = getPreDate('2022-11-14')
        expect(date?.day).toEqual(11)  
    })
})

describe('api.ts', () => {
    test('debug get5mData', () => {
        get5mData('SHOP', '2022-11-14')
    })
})

describe('sampleData.ts', () => {
    test('debug getSampleData', () => {
        const chartData = getSampleData('AAPL')
        expect(chartData.length).toBeGreaterThan(0)
    })
})

describe('symbolTrend.ts', () => {
    test('getSymbolTrend', () => {
        const data = getSymbolTrend('2022-11-14')
        expect(data).toBeDefined()
    })
})
