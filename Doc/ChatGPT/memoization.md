
## How could you cache execution of any function in javascript?

One way to cache the execution of any function in Javascript is by using a concept called memoization. Memoization is a programming technique that involves caching the results of a function so that future invocations of the function with the same arguments can be avoided, and instead, the cached results can be returned.

Here's an example implementation of a memoization function in Javascript:

```
function memoize(func) {
  const cache = {};

  return function(...args) {
    const key = JSON.stringify(args);

    if (cache[key]) {
      return cache[key];
    } else {
      const result = func.apply(this, args);
      cache[key] = result;
      return result;
    }
  }
}
```

In this implementation, `memoize` is a higher-order function that takes a function `func` as an argument and returns a new function that caches the results of `func`.


The inner function takes a variable number of arguments using the spread operator (`...args`) and converts them to a string using `JSON.stringify`, which is used as a key to the `cache` object. If the `cache` object already contains a result for that key, it returns the cached value. Otherwise, it calculates the value of `func` using `func.apply(this, args)` and caches the result before returning it.


Here's an example of how you can use `memoize` to cache the results of a function:


```
function factorial(n) {
  if (n <= 1) {
    return 1;
  } else {
    return n * factorial(n-1);
  }
}

const memoizedFactorial = memoize(factorial);

console.log(memoizedFactorial(5)); // calculates the result of 5! and caches it
console.log(memoizedFactorial(5)); // returns the cached result of 5! without recalculating it
```

In this example, `factorial` is a simple recursive function to calculate factorials. We create a memoized version of `factorial` using `memoize` and call it with an argument of `5`. The first time we call `memoizedFactorial(5)`, it calculates the result of `5!` and caches it. When we call `memoizedFactorial(5)` a second time, it returns the cached result of `5!` without recalculating it.

