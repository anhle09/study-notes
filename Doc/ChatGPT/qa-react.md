
## QA

How would you optimize the performance of a React application?
Can you describe the React Lifecycle methods and when to use them?
Have you worked with Redux or other similar state management frameworks? Can you explain how you used them in your project?
How do you handle asynchronous data in React? Have you used any libraries, such as Axios or Redux Thunk?
Have you worked with React hooks?
Have you implemented any testing frameworks in your React project, such as Jest or Enzyme? Can you describe your experience with them?

## Ex

For my most recent ReactJS project, I worked on building a task management application
where users could create, update, and delete tasks, as well as assign them to different categories.
The application also had a filtering feature which allows users to filter their tasks by category or sort them by deadline.

One of the challenges I faced during development was managing the application's state using Redux.
As the application grew in complexity and more features were added,
managing state across multiple components became increasingly difficult.
To tackle this, I spent time refactoring the application's state management using Redux,
which helped to centralize the state and made it easier to manage and maintain.

Overall, this was a challenging project that allowed me to improve my skills working with React, Redux,
and state management, as well as implementing optimization techniques to improve performance.

## Auth

In my experience, I have used JWT to handle authentication and authorization in a ReactJS application.

To authenticate users, I have used JWT that contain user information such as email address and user role.
When the user logs in to the application, the server generates a JWT and sends it back to the client,
which then stores the token in the browser's local storage. The token is then sent with every subsequent request made by the client to the server,
allowing the server to authenticate and authorize the user.

To authorize users, I have implemented role-based authorization, where each user has a role assigned to them based on their privileges.
For instance, an admin role may have access to all parts of the application while a regular user has access to only certain parts.
User roles are stored in a database, and are checked each time the user makes a request to the server.

Overall, using a combination of JWT and role-based authorization is an effective way of handling authentication and authorization in a ReactJS application.
