In the above example, we are testing two scenarios:

Renders a form input with a label
Renders an error message if the input is touched and invalid
In the first scenario, we are asserting that the input is rendered with the correct props,
and that we are able to enter a value into the input field.

In the second scenario, we are using Formik to validate the input field,
and we are asserting that the error message is displayed when the form is submitted without entering a value,
and that the error message disappears when a value is entered into the input field.


In the above `test` block, we created a mock function `mockAdd` using `jest.fn()`.
This function is equivalent to `add`, but any call to it will be recorded by Jest.

By using a mock function, we can isolate the function we want to test from its dependencies
and assert that it works correctly with those dependencies without actually calling them.
