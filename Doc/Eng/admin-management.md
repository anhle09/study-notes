##

Purpose of this page is to describe Access Management architecture 
and implementation delivered as part of the Assure solution. 
This document should serve as an input 
and overview for all the stakeholders including Developers 
which should use this document as a basis for 
their implementation design and development.

Functionality of the Access Manager is only available to DPU internally 
and delivered in form of a SDK which can be easily integrated 
with Assure Web Admin application but allowing 
for further integrations if required. 
The core functionality consists of:

##

User management
Add, Delete permissions
Add, Modify, Remove roles
Add, Modify, Revoke users
Including following self-care functionality
Password Change, reset
Scope Roles and Users per Environment
Grant, Revoke user access to individual applications

All the password or secrets stored as part of the 
Access Management must be encrypted using a symmetric key. 
This functionality is provided by integrating 
the Access Management with AWS KMS service 
which provide an API for symmetric crypto operations to 
encrypt and decrypt data. This includes

##

accessId - Random string used as a unique application identifier

accessKey - Random string which is encrypted 
at rest by a symmetric key from KMS. 
It's only returned once upon creation 
and not available afterwards

jwtExpiry - Expiration time in hours 
of JWT token which gets generated 
as part the authentication process

label - User friendly application name

kmsKeyId - ID of the private key 
from asymmetric key pair generated 
by AWS KMS upon Application record creation
Used to sign the JWT token

applicationType - Can be either DM (= Device Management) 
or KM (= Key Management) as it is essential 
to differentiate between the two for the compliance purposes

allowedOrigins - An array of allowed origins from 
where the authentication request can originate

publicKey - Public key from the key-pair used to generate the X.509 certificate
publicCert - Public certificate signed by rootCA 
from DPU account and stored as a string in the database

##

The created key-pair is used to sign 
and verify JWT token used for the Authentication process. 
The generated certificate (which is stored along with its public key) 
is used for mutual TLS authentication of the API calls
