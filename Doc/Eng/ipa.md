##

/ʊ/         oo, ou, u
/uː/	    oos, ue
/ʊə/ ua	    oor, ju

## A

/æ/ a	    a
/ɑː/ a	    ar
/aʊ/ ao     out, ow (allow)

/ʌ/ â	    o, u
/əʊ/ âu	    oa, ow
/eɪ/ ây	    a, ay
âp

## E

/e/         e
/eə/ eơ	    uar, air

## I

/iː/        ea, ee
/ɪ/         i
/ɪə/ ia	    ear, ere
it          d, t + (ed)
iz          s, sh, ch, c, g + (es)

## O

/ɔː/ o	    al, aw, or, ol
/ɒ/ o	    o
/ɔɪ/ oi	    oi, oy
/aɪ/ oai	ri, ry

/ə/ ơ	    na, er (a)
/ɜː/ ơ	    urn, ir
# ơt        art, ard
# ôt        oat, oad
ôp, ôn

##

/s/     ce
/ʧ/     church, match
/ʤ/     age, gym
/z/     zero, buzz
/ʃ/     ship, sure
/ʒ/     pleasure, vision

##

/iː/ 	sea, seen
/ɪ/	kid, bid, village
/ʊ/	good, put
/uː/	goose, blue
/e/	dress, bed
/æ/ a	trap, bad
/ɑː/ a	start, father
/ʌ/ â	come, love
/ɔː/ o	ball, law
/ɒ/ o	hot, box
/ə/ ơ	banana, teacher
/ɜː/ ơ	burn, birthday

/aʊ/ao	mouth, cow
/əʊ/âu	goat, show
/eɪ/ ây	face, day
/ɪə/ ia	near, here
/ɔɪ/ oi	choice, boy
/ʊə/ua	poor, jury

/eə/eơ	square, fair
/aɪ/oai	price, try


/ʧ/	church, match
/ʤ/	age, gym
/z/	zero, buzz
/ʃ/	ship, sure
/ʒ/	pleasure, vision