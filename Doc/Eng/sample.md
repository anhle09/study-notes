##

1. get added = is added

##

hello everyone, my name is LA, i am a new member, i am very happy to work with you all, thank you.
I am working as a web developer, I have 1 year of experience.

##

Yesterday, I implemented a feature that handles session expiration.
when the user log in, i get the application's expiration time, save it to local storage.
based on it, i calculate remaining session time and set timeout.
when the timeout is triggered, the application will log out.
that's all of me.

##

yesterday, i working on task-xxx, remove ability of admins to add and delete the var account in the UI.
specifically, i remove the delete link at the account list page and the account detail page,
remove DPU item of drop down at the account creating page.

##

The testing, of the implementation points, from Truong’s comment, have passed.
However, I observed, the following behaviors, which I discussed with the PO, and they are not, a concern:
1. when editing a Client account, and updating the account name:
AWS record has the attribute ‘type' = 'isClient’ removed
2. when editing a Client account, and removing an environment value
its record in the environment table, gets the value in the ‘accountId’ column, removed and set to NULL,
and when it gets added back, or another one gets added:
a new record gets added into the environment table, as well as into AWS
you can move it into Done.

##

Since VARs will be created, as part of the DevOps infrastructure task,
we need to remove, the ability for Admins, to add, or delete a VAR, from the DKMS UI.
The following rules apply:
A VAR account will be created through DevOps, the UI will not allow Admins, to create new VARs.
This can only be done, as an infrastructure task.
the UI will not, allow Admins to delete a VAR.
When a VAR account is deleted, all clients, device groups,
and devices will be deleted from AWS and the database.

Acceptance Criteria,
Remove the delete link, at the VAR level, in the Manage Accounts table,
Remove DPU, from the Parent drop down. Only clients of a VAR, can be added through the UI

##

When an existing user is at the DKMS login page
And clicks on the ‘Forgot Your Password’ hyperlink
And provides the correct and authorized email address
And clicks on the Search button
And provides the 2 correct answers to the challenge questions
And clicks on the Reset button
Then user should have his/her pwd reset and be able to successfully log in
Actual result:
user is presented with an error:
401 - Unauthorized
null value in column "isActive" of relation "user" violates not-null constraint
See attached screenshots for references.


1. provided the challenge question/answers for the user
2. on login, clicked on the ‘Forgot Your Password’ hyperlink
3. provided with the user email address
4. presented with the challenge questions, correctly answered to them
5. clicked on the ‘Reset’ button, user redirected back to, the login page,
and the following message, was correctly displayed, An email containing a new password has been sent.
6. since the email is not a real one, I logged in as DPU admin,
went into the Edit User, for the test user, and checked that pwd field, which was changed to a new pwd
7. copied and attempted to login with it, login was successful
This is safe to move into Done
Update 12/07,
I reattempted this, after creating a, new gmail account and:
From step 5: an email, ‘Reset Password’ was received from, dpu admin ses dev, containing a new random pwd
User successfully able to log in with the new pwd


1. As a DPU-Admin, I created a new user, with a valid email address, and setting up a pwd for it
Expected result: a AWS email, should have been sent, to that email address, with the setup pwd
Actual result: email was never received
2. As a new authorized user, I logged into DKMS, using the setup pwd,
and I was correctly displayed, with the Challenge Questions, and the Reset password button
When I clicked on the Reset PWD,
Then I should have been presented, with a way to setup my own pwd,
Actual result: I was immediately, redirected to the ASSURE DKMS homepage
See attached screenshot for reference.
It seems that the marked bit of, the Login flow doesn’t work as expected:


The UI drawer to create a new client, under a VAR has been changed.
Device Group Label field has been removed,
So the description steps, are no longer applicable.
1. Given I created a Client under the VAR,
When I return to the Account page,
Then record is there with, Account Name as entered,
inherited by the Parent along with, a Delete Action hyperlink,
Currently, Environment is blank, which makes the Client not appearing,
in the other screens, edit client and add environment
2. When I create a Device Group under that Client,
And I logout and back in to, the Device Inventory screen,
Then I expect that the Client, is created with its correct label ,
And the Device Group is created, underneath it with the correct label,
This is now behaving, as expected and no longer an issue.
