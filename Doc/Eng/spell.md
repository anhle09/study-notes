##

1. ko biet nhan thi thoi ko nhan, doc ngang (ko dau huyen, sac, nang), mieng tron rong
2. am cuoi doc dau huyen/nang, nhan doc dau sac, con lai doc ngang
3. hoc phien am cac key word, con lai bo qua
4. ko co "ô, ê", nhieu "ơ" (al ~ oô)
5. as -> as; an -> en; ce -> s; e, i, u -> ơ; o -> au
6. tu 2 am: dong tu nhan am 2 -> con lai thi nhan am 1
7. tu nhieu am: thuong nhan am 2 (nhan -> doc am tieng viet)

## thu tu danh van

1. van ghep
2. theo dau sac (t, d, k)
3. ko ghep

## A

- ơ     dung dau, an, ra
- a     ghep + con lai, dau sac
- e     ar
- ây    ai, ay, ge, ga
- ây    âm 1 ko ghep
- o     al, aw

## E

- i     ko nhan
- e     nhan, nhan-2, (em, en, es)
- ơ     er, ar
- ây    ei, ey

## I

- ơ     ir, ko nhan
- i     ghep + con lai
- ai    ie, i_e, iz, ri, dung dau ko ghep

## O

- o     nhan
- ơ_â   ko nhan

## U:

- â, iu
- ơ     ur

## Note

1. Silent letter: _e
2. Phu am ghep tu: c, d, g, k, l, m, n, t, x
3. Phu am ko ghep tu: s, v
4. the + phu am -> đi
5. gerund: danh dong tu (v-ing)
