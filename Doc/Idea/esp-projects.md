** ez engineering channel
- easy, simple
- beautiful, fun
- workable

--- office time

** tsf dk xe bang ki hieu tay (recognize)
- esp32
- 2 servo 360
- pin

** ball balancing (100k)
- esp32
- 3 servo
- mica

** tsf tuxi (recognize)
- esp32
- 2 servo
- cardboard hand

** tsf control arm

--- hard (high cost)

** balancing robot

** bottle chinook
- esp8266
- motor + canh, 2N2222 (50k)
- servo (60k)
- mpu 6050 (30k)
- pin (100k)

** fpv tank (toothpick gun)
- 4 servo
- lo xo hot quet

--- easy

** upgrade switch, fan
- 1 servo
- web ui

--- mobile app

** kid car: acc sensor, forza horion
- esp32
- mo hinh
- motor driver
- sac pin

** acc sensor control cctv

--- hardest

- drawing machine
- balancing bike

---

** key words: build, simple, esp32

** others proj
- pid: quadcopter
- esp: internet, all pin
- self driving: den do, toc do ??
- air defence gun
- rc ninja h2
- game vehicle: forza horizon, battlefield
- kid car: can cau, forza horion, tank
