
## rest api

short for: REST is short for representational state tranfer.
standard: it is a set of rules and a standard for building web API.
follows: an api follows the rest standard is called a restful api.

## browser

you enter a url into the browser and hits enter, what happens next?

dns: the browser get ip address of server from DNS, DNS stands for (is short for) domain name system.
to make the lookup process fast, the DNS information is cached.
if not found in cache, the browser make a query out to the internet.

tcp connection: when the browser has the ip address, it will establish a tcp connection to server.
then it sends a http request to the server over the established tcp connection.

response: the server processes the request and send back a response.
the browser receives response and render (display) html content.

## design pattern

React: HOC, render props
Nest: singleton, dependency injection

## How do you keep up with industry trends and new front-end technologies?

I follow several youtube channels about programming, it gives me a lot of new information.

## What are some best practices to writing clean and maintainable code?

Kiss, Dry
Use consistent coding conventions
Break code into smaller modules

##

- Because I like it and I have the ability to do it well
- I don't use it in my code, so I have no idea about it.
- Can i skip this question, i'm not strong about it.
