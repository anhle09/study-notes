
# database

Db normalization is a process used to organize a db into tables and columns
the idea is that a table should be about a specific topic
this limits duplicate data within db

Db denormalization is opposite of normalization. it is the process of
adding redundant data to db to improve read performance.
this is done by adding duplicate data to multiple table
to avoid expensive joins.
it will increase storage and decrease write performance

Locking is used to prevent data from being modified
by multiple processes at the same time.
this is very important because
if two processes are modifying the same data at the same time
data can be corrupted, locks is used to prevent this from happening


# usecase diagram

it is used to model the interactions between the actors and the system
an actor is a person or a system that interacts with the system
a usecase is a task that the system performs
