mr a, it's ready to interview you now
first I'd like to congratulate you on passing our aptitude test
i saw an advertisement online
i clicked on it and it brought me to your company's website, where i applied for it

what do you know about our company so far
i've read on your website that for over ten years
LCI has delivered profestional services on software development project for clients all over the world
we always try to do quality work

i have a few official software certificates
i started by building websites for people i knew
it helped me build a portfolio of projects
i got my first IOS development job at a software agency called Tech Services

i had the opportunity to learn more about the software building process there
but, since there were not too many projects at that agency, after 2 years, i decided to look for another job
i soon stared to work at an international startup company, soft solutions
it was a good chance to join a team
i've been able to use my training and experience fully at this company
i gained experience in many things like teamwork, time management, and quick decision making

how long have you been working for soft solutions
i've been working there for three years
why do you want to leave that job
i'm looking for a company where i can contribute more and grow
i'm ready to take the next step of my career

what exactly made you apply for this job at our company
i can see that your company has much future growth potential
i would love to grow my career at a company that has a great reputation in the software industry

what are your strengths
i'm a great team player
i always tried to help my teammates complete their tasks
if i had completed mine whenever someone needed to leave earlier
i would offer to cover for them
i'm willing to do whatever it takes for the team or company to succeed

what are your weaknesses
i can be a bit too straightforward at times and sometimes that hurts people
but i'm working on this and try to stop myself from being overly direct

where do you see yourself in five years
in five years i'd like to be seen as someone with deep expertise in software development
i hope to be offered the opportunity to take the lead on some projects
there are definitely promotion opportunities at our company depending on your achievements and expertise

do you work well under pressure usually
i don't panic and manage to maintain self-control
there were many tight deadlines and stressful situations at my previous jobs
i always put in extra hours and did my best in meeting the needs of the clients
i've always delivered high quality work and respected my deadlines interesting

can you tell me about a recent accomplishment or success you had
i was part of the team that built the lewis kitchen app
with this app it's easy to order your favorites even when you're on the go
i love creating something that people can interact with and enjoy

that's quite impressive, i must admit okay, thanks so much for coming in
it was great to meet you, i'll give you a call tomorrow
sounds good, i look forward to it, thank you

hello, this is tom davis, the manager of lci software
am i speaking to mark taurus
yes this is mark torres speaking
great i'm calling to inform you that your interview was successful and i'd like to offer you the job
thank you so much i'd love to take the job

##

allowing you to easily combine js
if u want to pass data into a component, u simply pass it a props argument which

if the value changes react will react to update the ui
if we want to give our component its own internal state, we can use the state hook
the hook is just a function that returns a value as well as a function to changes the value
we can bind set count to button click event so the user can change the state

react provides a variety of other built-in hooks to handle common use cases
but the main reason you might want to use react is not the library itself but
the massive ecosystem that surrounds its
