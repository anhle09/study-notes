
## introduce

strong foundation in: i have strong foundation in developing web application using Html, Js, Ts, Css.

have worked: i have worked with frameworks such as React, Vue, Express, NestJs.

experience in: i have experience in developing user-friendly web applications, working with version control tools like Git.

understand: i understand the importance of clean, structured, and reusable code.

familiar with: i'm familiar with KISS and DRY principles.

I'm confident that I can make a valuable contribution to your team.
I'm eager to continue learning and developing my skills.

## junior backend

As a Junior Back End Developer, I have a solid foundation in creating functional and efficient web applications.
I am experienced in server-side programming languages such as NodeJs, Java
and working with databases like PostgreSQL and MongoDB.

My previous projects have given me experience in developing APIs, creating server-side APIs endpoints,
implementing application security, and working with cloud services like AWS.

I am knowledgeable in web development concepts like RESTful APIs, HTTP protocol, and SQL queries.
I am comfortable with version control systems such as Git, and have utilized tools such as Postman to test and debug APIs.

As a Junior Back End Developer, I am eager to learn new technologies, frameworks, and programming languages.
I understand the importance of clean, structured, and reusable code, and I am committed to following best practices.

I work well in collaborative team environments, and am willing to learn from and contribute to other developers.
I am dedicated to continuous learning and self-improvement in order to become a valuable member of any development team.

Overall, I am excited about the potential of growing as a developer
and look forward to continuing to develop my skills in back-end development.

## company

I have worked for Qt, it is a software outsourcing company in Hcmc, Vietnam.
my company have over 70 employees,
we are providing services to our customers in Us, Australia, Japan.
I am responsible for analyzing, designing, developing, and implementing web application.

i study in hcm university of technology and education,
my major is electronics and telecommunication.

## Main of Job

Collaborated with a team members to build and maintain websites.
Develop back-end to integrate front-end functionality.
Ticketed and resolved bugs. Performed testing and optimization.
Write reusable and easily maintainable code. Participate in code reviews and testing to ensure high quality and maintainable code.
Debug and troubleshoot issues and help provide solutions to improve performance and user experience.
Participate in Agile/Scrum processes and daily stand-up meetings to ensure efficient progress and timely delivery of projects.
