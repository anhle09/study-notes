# hoisting

move to the top: hoisting is a behavior where variable and function declarations are moved to the top of their scopes.
use before declared: this means that you can use a variable or function before it has been declared,
as long as it's within the same scope.

# event delegation

single-parenet: event delegation is a technique where you attach a single event listener to a parent element,
multiple-children: instead of attaching multiple event listeners to individual child elements,
this can improve performance and simplify your code.

# event delegate ??

bubbling: when an event happens on the element,
the event handler will run on it first, then running on its parent.
capturing: opposite with bubbling (the reverse of).

the method for stopping bubbling is event.stopPropagation()

## Explain equality in JavaScript

there are two types of equality: strict equality and loose equality.
Strict equality (===) compares both the value and the data type of two operands,
while loose equality (==) only compares their value.

## DOM

programming interface: DOM is a programming interface used to interact with HTML documents.
tree of objects: It represents the structure of a web page as a tree of objects, where each object represents an HTML element.
manipulate: By using the DOM API, we can use JavaScript to modify, add or remove html elements.

## How do you approach testing your front-end code, and what tools do you use?"

my front-end testing approach include unit testing, integration testing, cross-browser testing, and performance testing.
By using a combination of these tools in my testing process,
I can ensure that the front-end code is optimal for end-users and delivers a seamless experience.

## Promise

a Promise is an object representing an asynchronous operation.
Promises are used for handling asynchronous operations such as API requests, database queries, and more.

The states of Promise are "pending" and "resolve/reject".
When a Promise is "pending", it means that the asynchronous operation has not yet completed.
When a Promise is "resolve/reject", it means that the asynchronous operation has completed successfully or failed.

When creating a Promise, the `new Promise()` constructor expects a function with two parameters `resolve` and `reject`.
These functions are used to indicate whether the asynchronous operation succeeded or failed.

To handle the results of a Promise, you can use the `.then()` method.
The `.then()` method takes a function as its parameter, which will be called when the Promise resolves successfully

To handle a failure case, you can use the `.catch()` method.
The `.catch()` method takes a function as its parameter, which will be called if the Promise is rejected

## ES

ES5, ES6 is the ECMAScript standard

ES5 introduced several new features including:
- JSON parsing and serialization (parse, stringify)
- Array methods such as map, filter, forEach

Some of the new features introduced in ES6 include:

- Let and const keywords: It declare variables with block scope.
`const` is used to declare a variable that cannot be re-assigned (constant),
while `let` is used to declare a variable with a re-assignable value.

- Arrow functions
- Promises
- Classes and modules (import, export)
- Spread operator: used to spread an array or object into another array or object.
- Destructuring: used to extract values from arrays and objects.
