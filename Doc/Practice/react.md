## what is react
react is an open-source front-end javascript library that is used to build ui,
specially for single page app. it was developed by fb.

## what are the major feature of react?

it uses virtual dom instead of real dom
supports server side rendering
follows the unidirection data flow and data binding
uses reusable ui component

## Can you explain the difference between Props and State in React?

pass data down: props are used to pass data down to child components, the child component can not modify props
manage data within: state is used to manage data within a component, the child component can modify state

## Virtual DOM

lightweight copy: The virtual DOM in React is a lightweight copy of the actual DOM.
updates the parts: React only updates the parts of the DOM that need to be changed,
making the application faster and more efficient.

## HOC

component as an argument: HOC is functions that accept a component as an argument
and return a new component that wraps the original component

We can reuse code and add new features to your components without having to modify their existing code.
It avoid duplicating code or applying the same functionality to multiple components.

## How would you optimize the performance of a React application?

Use library such as react-virtualized or react-window to render long lists efficiently by rendering only the visible content.
Use React.memo() to prevent unnecessary re-renders of components.
Reduce page load time: optimize images and other assets, use lazy loading and code splitting.
Use server-side rendering (SSR) to pre-render the initial markup and reduce the time to first contentful paint (FCP).

## Explain the differences between cookies, sessionStorage, and localStorage

store data: They are all used to store data in the web browser,
small data + expiration: cookies store small data with an expiration date,
data temporarily: sessionStorage stores data temporarily during the current browsing session,
larger data + persistent: localStorage stores larger data which is persistent even after the browser is closed.

## Redux

state management: Redux is a state management library to help manage application state
in large and complex React applications. The core principle of Redux is that:

object that holds: Store is a single JavaScript object that holds the application state.
The store is immutable and can only be updated by dispatching an action to it.

object that describes: An action is an object that describes what happened in the application,
including the type of the action and any data associated with it.

functions that handle: Reducers are functions that handle different types of actions and return a new version of the state.

specific piece: Selectors are functions that take the current state and return a specific piece of information from it

additional logic: Middleware used to additional logic to be added to the dispatch process.
It can be used for tasks such as logging, asynchronous requests, and routing.

To use Redux in a React application, you would typically have a top-level component
that is connected to the Redux store using the `connect` function from the `react-redux` library.
This component would then pass state and action dispatch functions down to child components as props.

## SSR

technique of rendering: Server-side rendering (SSR) is the technique of rendering React components
on the server instead of the browser. This can help improve page load speeds, SEO optimization.

## Auth

In my experience, I have used JWT to handle authentication and authorization in a ReactJS application.

To authenticate users, I have used JWT that contain user information such as email address and user role.
When the user logs in to the application, the server generates a JWT and sends it back to the client,
which then stores the token in the browser's local storage.
The token is then sent with every request made by the client to the server,
allowing the server to authenticate and authorize the user.

To authorize users, I have implemented role-based authorization,
For instance (example), an admin role may have access to all parts of the application
while a regular user has access to only certain parts.
User roles are stored in a database, and are checked each time the user makes a request to the server.

Overall, using a combination of JWT and role-based authorization is an effective way of handling authentication
and authorization in a ReactJS application.

## Challenge

My previous ReactJS project, I build a task management application,
where users could create, update, and delete tasks.
The application also had a filtering/sorting feature which allows users to filter/sort their tasks by category.

One of the challenges I faced during development was managing the application's state using Redux.
As the application grew in complexity and more features were added,
managing state across multiple components became increasingly difficult.
I spent time refactoring the application's state management using Redux,
which helped to centralize the state and made it easier to manage and maintain.

Overall, this was a challenging project, it helps me to improve my skills working with React, Redux.
