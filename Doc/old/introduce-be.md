
## Junior

As a Junior Back End Developer, I have a solid foundation in creating functional and efficient web applications.
I am experienced in server-side programming languages such as NodeJs, Java
and working with databases like MySQL, MongoDB or PostgreSQL.

My previous projects have given me experience in developing APIs, creating server-side APIs endpoints,
implementing application security, and working with cloud services like AWS.

I am knowledgeable in web development concepts like RESTful APIs, HTTP protocol, and SQL queries.
I am comfortable with version control systems such as Git, and have utilized tools such as Postman to test and debug APIs.

As a Junior Back End Developer, I am eager to learn new technologies, frameworks, and programming languages.
I understand the importance of clean, structured, and reusable code, and I am committed to following best practices.

I work well in collaborative team environments, and am willing to learn from and contribute to other developers.
I am dedicated to continuous learning and self-improvement in order to become a valuable member of any development team.

Overall, I am excited about the potential of growing as a developer
and look forward to continuing to develop my skills in back-end development.
