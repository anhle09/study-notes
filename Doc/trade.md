## uptrend reasons
- btc, nvda, vcb tang -> tinh hinh da tot hon
- chu ki giam da keo dai, phan tram giam da nhieu
- tinh hinh pha san, dich benh, chien tranh on dinh
- co the ko tang manh, se co dieu chinh, nhung van la tang

## note
- entry luc nao cung se co cam giac "co giam nua hay ko", giam thi cat thoi
- nen do la de vao, nen xanh la de chot ??
- vao nhanh, thoat nhanh, roi neu tinh hinh tot thi vao lai

## entry
1. Diem vao (vung can)
- Giua rau nen tuan can truoc do, co 3 nen ngay tranh chap
- Fibonanci, rsi + macd (2 duong trung binh cat nhau)
- Tin hieu khac (aapl, vcb), nen tuan manh (hoac om tron nen truoc)

2. Cach vao
- Tin hieu yeu (macd + rsi), vung cao: vao moi cuoi tuan x 5 lan
- Vung can thap: vao moi cuoi tuan x 5 lan
- Tin hieu nhanh, vung thap: so lan vao it hon (moi 2 ngay x 5 lan)
- Cat khi sai huong (nen tuan -7%), roi mua lai gia thap hon
-> neu tang la trung binh gia van ngon, neu giam thì chi vao 1 phan nen rui ro thap

3. Vung cao
- Nen do lan thu 3 ma ko vuot dinh cu thi cat (btc thi nen tuan thu 2)
- Ngay thu 5, 6 neu nen tuan giam -7% thi cat het, cho tin hieu vao lai
- Nen tuan tiep theo la nen manh thi vao 5 lan moi tuan
-> neu tang thi van ngon, neu giam thi da cat cang ngon

## note
- Khong nghe theo tin tuc, tap trung vao ky thuat, hinh nen
- Vn khung tuan tin hieu se manh, chi test 2 lan, ko nhay, clear hon
- Lai suat giam, vi mo tot hon, nhom nganh tai chinh, bds
- Quy tac dong tien, thi truong + nhom nganh
- Song 1 moment manh, song 2 ru bo, song 3 day gia roi phan phoi
- Mo hinh 3 dinh, vai dau vai, tang manh se giam manh roi se tang manh (bien do)
- Neu miss song 1 thi cho vao song 2, lauchpad, short
- Nen ngay chi tham khao, chap nhan rui ro cho toi cuoi tuan

## chu ki
- Chu ki 2 nam: 1 nam tang, 1 nam giam (co the +1 nam side way, roi chuyen qua chu ki 4 nam)
- Chu ki 4 nam: 2 nam tang (se co dieu chinh roi tang tiep), 2 nam giam (se co dieu chinh roi giam tiep)
- Sideway thi trung binh gia

## btc
- flow dw: oth > dw1_sw > re > dw2_sw > re > dw_sw otl
- flow up: otl > up1 > re > up2 > oth (xay ra nhanh, de bi miss)
- flow: flow dw otl > flow up > flow dw > flow up oth
- song hoi: hinh non nhon, chi hoi 1 chut roi tiep tuc, ko co dao chieu
- ko phai song hoi: hinh cung, moment manh, co tranh chap dao chieu (ko sw)
- nen 4h chi tham khao, chap nhan rui ro cho toi cuoi ngay
-> vao khi test can va co ~3w sideway, vao lenh cuoi ngay, 5 lan moi ngay

## Cfd: 1h-2h, bien do, duong nen, cum nen, sl 4d
