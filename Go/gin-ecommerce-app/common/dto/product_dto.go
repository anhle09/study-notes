package dto

import "main/model"

type ProdCountDto struct {
	BrandId int         `json:"brandId"`
	Total   int         `json:"total"`
	Brand   model.Brand `json:"brand"`
}

type InventoryStore struct {
	StoreName string `json:"storeName"`
	Total     int    `json:"total"`
}

type DeviceCountDto struct {
	Name  string `json:"name"`
	Total int    `json:"total"`
	// Sold  int    `json:"sold"`
	// InventoryStore []InventoryStore `json:"inventoryStore"`
	// ProductUrl     string           `json:"productUrl"`
}

type DeviceSummaryDto struct {
	VariantName    string `json:"variantName"`
	Total          int    `json:"total"`
	InventoryCount int    `json:"inventoryCount"`
}
