package middleware

import (
	"strings"

	"github.com/gin-gonic/gin"
)

func Authenticate(role string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var bearer = ctx.Request.Header.Get("Authorization")
		var token = strings.Split(bearer, " ")[1]

		if token != role {
			ctx.AbortWithStatus(401)
		}
	}
}
