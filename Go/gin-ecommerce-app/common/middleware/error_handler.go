package middleware

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func ErrorHandler(ctx *gin.Context) {
	ctx.Next()
	if ctx.Errors != nil {
		var err = ctx.Errors.String()
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err})
	}
}
