package seed

import (
	"main/model"
	"math/rand"
	"strconv"
	"time"
)

const Apple, Samsung, Xiaomi = 1, 2, 3   // brand id
const Mobile, Laptop, Tablet = 1, 2, 3   // product group id
const Cpu, Rom, Ram = 1, 2, 3            // spec group id
const CpuBrand, RomCap, RamCap = 1, 2, 3 // spec attribute id
const Hanoi, Saigon, Danang = 1, 2, 3    // store

// CpuIds = [1, 2, 3] RomIds = [4, 5, 6], RamIds = [7, 8, 9] // spec id

func random(min int, max int) int {
	return rand.Intn(max-min) + min
}

func createMockSpec(brand string) []model.ProductSpecMapping {
	var cpuBrand = map[string]int{
		"Apple":   1,
		"Samsung": 2,
		"Xiaomi":  3,
	}
	return []model.ProductSpecMapping{
		{SpecId: cpuBrand[brand]},
		{SpecId: random(4, 7)},
		{SpecId: random(7, 10)},
	}
}

// require rand.Seed(time.Now().UnixNano())
func createMockProd(groupId int, brandId int, brandName string) model.Product {
	var prodName = brandName + " Product " + strconv.Itoa(random(100, 200))
	return model.Product{
		ProductGroupId: groupId,
		BrandId:        brandId,
		Name:           prodName,
		Price:          random(50, 150) * 10,
		Sku:            strconv.Itoa(random(200000, 300000)),
		SpecIds:        createMockSpec(brandName),
	}
}

func CreateMockProduct(groupId int, brandId int, brandName string) model.Product {
	rand.Seed(time.Now().UnixNano())
	return createMockProd(groupId, brandId, brandName)
}

func CreateMockProductList(groupId int, brandId int, brandName string, num int) []model.Product {
	rand.Seed(time.Now().UnixNano())
	var prodList []model.Product

	for i := 0; i < num; i++ {
		prodList = append(prodList, createMockProd(groupId, brandId, brandName))
	}
	return prodList
}

func CreateMockDeviceList(prodId int, num int) []model.Device {
	rand.Seed(time.Now().UnixNano())
	var deviceList []model.Device

	for i := 0; i < num; i++ {
		deviceList = append(deviceList, model.Device{
			ProductId: prodId,
			Serial:    strconv.Itoa(random(100000, 200000)),
		})
	}
	return deviceList
}
