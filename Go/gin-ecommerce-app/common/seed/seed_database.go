package seed

import (
	"main/model"
)

func SeedDatabase() {
	model.ConnectDatabase()
	model.DB.Migrator().DropTable(
		&model.ProductGroup{},
		&model.Brand{},
		&model.ProductVariant{},
		&model.Device{},
		&model.Product{},
		&model.SpecGroup{},
		&model.SpecAttribute{},
		&model.Spec{},
		&model.ProductSpecMapping{},
		&model.Store{},
		&model.Address{},
		// &model.User{},
	)
	model.MigrateDatabase()

	model.DB.Create(&[]model.ProductGroup{
		{Name: "Mobile"},
		{Name: "Laptop"},
		{Name: "Tablet"},
	})
	model.DB.Create(&[]model.Brand{
		{ProductGroupId: Mobile, Name: "Apple"},
		{ProductGroupId: Mobile, Name: "Samsung"},
		{ProductGroupId: Mobile, Name: "Xiaomi"},
	})
	model.DB.Create(&[]model.SpecGroup{
		{ProductGroupId: Mobile, Name: "CPU"},
		{ProductGroupId: Mobile, Name: "ROM"},
		{ProductGroupId: Mobile, Name: "RAM"},
	})
	model.DB.Create(&[]model.SpecAttribute{
		{SpecGroupId: Cpu, Name: "Brand"},
		{SpecGroupId: Rom, Name: "Capacity"},
		{SpecGroupId: Ram, Name: "Capacity"},
	})
	model.DB.Create(&[]model.Spec{
		{SpecGroupId: Cpu, SpecAttributeId: CpuBrand, Value: "Apple", IsOverview: true},
		{SpecGroupId: Cpu, SpecAttributeId: CpuBrand, Value: "Samsung", IsOverview: true},
		{SpecGroupId: Cpu, SpecAttributeId: CpuBrand, Value: "Qualcomm", IsOverview: true},
		{SpecGroupId: Rom, SpecAttributeId: RomCap, Value: "128"},
		{SpecGroupId: Rom, SpecAttributeId: RomCap, Value: "256"},
		{SpecGroupId: Rom, SpecAttributeId: RomCap, Value: "512"},
		{SpecGroupId: Ram, SpecAttributeId: RamCap, Value: "4"},
		{SpecGroupId: Ram, SpecAttributeId: RamCap, Value: "8"},
		{SpecGroupId: Ram, SpecAttributeId: RamCap, Value: "16"},
	})
	model.DB.Create(&[]model.Store{
		{Name: "Hanoi Store"},
		{Name: "Saigon Store"},
		{Name: "Danang Store"},
	})
	model.DB.Create(CreateMockProductList(Mobile, Samsung, "Samsung", 100))
	model.DB.Create(CreateMockDeviceList(1, 10))
}
