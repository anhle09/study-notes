package util

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

type TestCase struct {
	Method   string
	Url      string
	Token    string
	Body     any
	Expected int
}

type TestGroup map[string]TestCase

func objectToBuffer(obj any) *bytes.Buffer {
	b, _ := json.Marshal(obj)
	return bytes.NewBuffer(b)
}

func RunTest(e *gin.Engine, a *assert.Assertions, t TestCase, msg string) {
	var body = objectToBuffer(t.Body)
	var req, _ = http.NewRequest(t.Method, t.Url, body)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", t.Token)

	var res = httptest.NewRecorder()
	e.ServeHTTP(res, req)
	a.Equal(t.Expected, res.Code, msg)
}

func RunTestGroup(e *gin.Engine, a *assert.Assertions, t TestGroup) {
	for key, testData := range t {
		RunTest(e, a, testData, key)
	}
}
