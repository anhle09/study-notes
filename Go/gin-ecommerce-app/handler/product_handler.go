package handler

import (
	"net/http"
	"strconv"

	"main/model"
	"main/service"

	"github.com/gin-gonic/gin"
)

func RegisterProdRoutes(router *gin.RouterGroup) {
	router.POST("/product", createProduct)
	router.GET("/product/:id", getProduct)
	router.GET("/products", getProducts)
	router.PUT("/product", updateProduct)
	router.DELETE("/product", deleteProduct)
}

func createProduct(ctx *gin.Context) {
	var prod model.Product
	var err = ctx.BindJSON(&prod)
	if err != nil {
		ctx.Error(err)
		return
	}
	// err = model.DB.Create(&prod).Error
	err = service.Product.Create(&prod)
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": prod.Id})
}

func getProduct(ctx *gin.Context) {
	var prodId, paramErr = strconv.Atoi(ctx.Param("id"))
	if paramErr != nil {
		ctx.Error(paramErr)
		return
	}
	var prod, specs, err = service.Product.Detail(prodId)
	if err != nil {
		ctx.Error(err)
		return
	}
	var res = gin.H{"product": prod, "specs": specs}
	ctx.JSON(http.StatusOK, gin.H{"data": res})
}

func getProducts(ctx *gin.Context) {
	var orderBy = ctx.Query("orderBy")
	var page = ctx.Query("page")
	var prodList, err = service.Product.Query("", []any{}, orderBy, page)
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": prodList})
}

func updateProduct(ctx *gin.Context) {
	var prod model.Product
	var err = ctx.BindJSON(&prod)
	if err != nil {
		ctx.Error(err)
		return
	}
	err = service.Product.Update(&prod)
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.Status(http.StatusOK)
}

func deleteProduct(ctx *gin.Context) {
	var prodId, paramErr = strconv.Atoi(ctx.Param("id"))
	if paramErr != nil {
		ctx.Error(paramErr)
		return
	}
	var err = service.Product.Delete(prodId)
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.Status(http.StatusOK)
}
