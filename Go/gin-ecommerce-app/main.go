package main

import (
	"net/http"

	"main/handler"
	"main/model"

	"github.com/gin-gonic/gin"
)

func main() {
	model.ConnectDatabase()

	gin.SetMode(gin.ReleaseMode)
	var app = gin.Default()
	var router = app.Group("/api")
	handler.RegisterProdRoutes(router)
	handler.RegisterProdGroupRoutes(router)

	app.GET("/ping", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "pong")
	})

	app.Run() // ":3000"
}
