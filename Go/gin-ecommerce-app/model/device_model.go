package model

type Device struct {
	Id               int    `json:"id"`
	ProductId        int    `json:"productId"`
	StoreId          int    `json:"storeId,omitempty"`
	ClientId         int    `json:"clientId,omitempty"`
	ProductVariantId int    `json:"productVariantId,omitempty"`
	Serial           string `json:"serial"`
	Status           string `json:"status,omitempty"`
	ActiveAt         string `json:"activeAt,omitempty"`
	// ProductVariant   ProductVariant `json:"productVariant,omitempty"`
}
