package model

type NavFilter struct {
	Id            int    `json:"id"`
	FilterGroupId int    `json:"filterGroupId"`
	Name          string `json:"name"`
	Query         string `json:"query"`
	Slug          string `json:",omitempty"`
	Seo           string `json:",omitempty"`
}

type FilterGroup struct {
	Id             int         `json:"id"`
	ProductGroupId int         `json:"productGroupId"`
	Name           string      `json:"name"`
	NavFilters     []NavFilter `json:"navFilters" gorm:"ForeignKey:FilterGroupId;"`
}
