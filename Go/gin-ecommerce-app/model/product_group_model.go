package model

type ProductGroup struct {
	Id   int    `json:"id"`
	Name string `json:"name" gorm:"not null" binding:"required"`
	Slug string `json:",omitempty"`
	Icon string `json:",omitempty"`
	// FilterGroup []FilterGroup `json:"filters,omitempty" gorm:"ForeignKey:ProductGroupId;"`
}

type ProductType struct {
	Id             int    `json:"id"`
	ProductGroupId int    `json:"productGroupId"`
	Name           string `json:"name"`
	Slug           string `json:",omitempty"`
	Icon           string `json:",omitempty"`
}

type Brand struct {
	Id             int    `json:"id"`
	ProductGroupId int    `json:"productGroupId" gorm:"not null"`
	Name           string `json:"name" gorm:"not null"`
	Slug           string `json:",omitempty"`
	Icon           string `json:",omitempty"`
}
