package model

type Product struct {
	Id             int                  `json:"id"`
	ProductGroupId int                  `json:"productGroupId" binding:"required"`
	ProductTypeId  int                  `json:"productTypeId"`
	BrandId        int                  `json:"brandId" binding:"required"`
	Brand          Brand                `json:"brand"`
	Name           string               `json:"name" binding:"required,min=3"`
	Price          int                  `json:"price" binding:"required"`
	Sku            string               `json:"sku" binding:"required"`
	SpecIds        []ProductSpecMapping `json:"specIds" binding:"required"`
	Specs          []Spec               `json:"specs" gorm:"many2many:ProductSpecMapping;"`
	Variants       []ProductVariant     `json:"variants,omitempty"`
	Devices        []Device             `json:"devices,omitempty"`
	Status         string               `json:",omitempty"`
	Description    string               `json:",omitempty"`
	Images         string               `json:",omitempty"`
	Discount       string               `json:",omitempty"` // Discount
	Promotions     string               `json:",omitempty"` // []Promotion
	Comments       string               `json:",omitempty"` // []Comment
	CreatedBy      string               `json:",omitempty"` // User
	Rating         string               `json:",omitempty"`
	Slug           string               `json:",omitempty"`
	Seo            string               `json:",omitempty"`
}

type ProductVariant struct {
	Id        int    `json:"id"`
	ProductId int    `json:"productId"`
	Name      string `json:"name" binding:"required,min=3"`
	Price     int    `json:"price" binding:"required"`
	// Specs     []Spec `json:"specs" binding:"required" gorm:"many2many:ProductSpecMapping;"`
}

type ProductSpecMapping struct {
	ProductId int `gorm:"primaryKey"`
	SpecId    int `gorm:"primaryKey"`
}
