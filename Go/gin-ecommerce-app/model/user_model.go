package model

type User struct {
	Id       int     `json:"id"`
	Email    string  `json:"email"`
	Password string  `json:"password,omitempty"`
	Name     string  `json:"name,omitempty"`
	Status   string  `json:"status,omitempty"`
	Image    *string `json:"image,omitempty"`
}

// type User struct {
// 	Id       uint
// 	Email    string
// 	Password string
// 	Name     string
// }
