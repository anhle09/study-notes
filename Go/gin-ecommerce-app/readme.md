## json

- json.Marshal(obj) -> bytes
- json.Unmarshal(bytes, &struct) -> not &any (-> res.data...)

## binding

- ctx.Bind(&payload) -> case-insensitive
- ctx.JSON(200, gin.H{"data": data}) -> case-sensitive
-> map by json tag name

## li do co spec model

- tranh trung lap
- de render ui (group)
- de them spec item (tinh nang cua sp)
- moi sp co spec khac nhau

## product client api (public): mobile ex

> get mb list (pagination): thumbnail, name, spec (some), variants
> filter: brand, price, feature, pin, display, camera
> sort: popular, price, discount
> get mb detail + spec group + attribute
> search product inventory: by name, serial
- get store inventory

## create prod page

> seed prod group, brand, spec
> get spec form (group, attribute, value) by prodGroupId, prodTypeId
> create product (coming soon)
> create instance
> update instance -> ship to store -> release 
> update instance -> ship to customer -> sold
> update instance -> return, check

# dashboard (group by prod group)

> list brand by prod group -> []{brandName, count}
> list prod by brand -> []{prodName, inventoryCount} -> get prod by brand, count device by prod
> -> join prod + device -> count device, group by prod
- list device by prod -> []device, []{variantName, inventoryCount} -> get device by prod

# docker

- docker build --tag go-ecm .
- docker run -d -p 8080:8080 go-ecm
