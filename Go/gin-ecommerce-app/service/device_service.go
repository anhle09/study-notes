package service

import (
	"main/model"
)

type deviceService struct{}

func (s *deviceService) CreateMany(prod *[]model.Device) error {
	return model.DB.Create(prod).Error
}

/* query "product_id = ? | store_id = ? | serial LIKE ?"
 */
func (s *deviceService) GetMany(query string, args []any) ([]model.Device, error) {
	var prod []model.Device
	var err = model.DB.Where(query, args...).Find(&prod).Error
	return prod, err
}

func (s *deviceService) ShipToStore(storeId int, prodIds []int) error {
	return model.DB.Where("id IN ?", prodIds).Updates(model.Device{StoreId: storeId}).Error
}

func (s *deviceService) ShipToClient(clientId int, prodId int) error {
	return model.DB.Updates(model.Device{Id: prodId, ClientId: clientId}).Error
}
