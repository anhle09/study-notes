package service

import (
	"main/common/dto"
	"main/model"
	"strconv"
)

type productService struct{}

func (s *productService) Create(payload *model.Product) error {
	var err = model.DB.Create(payload).Error
	return err
}

func (s *productService) Detail(id int) (model.Product, []model.SpecGroup, error) {
	var prod model.Product
	var err = model.DB.Preload("Specs").First(&prod, id).Error
	if err != nil {
		return prod, []model.SpecGroup{}, err
	}
	var specGroup []model.SpecGroup
	err = model.DB.Where("product_group_id = ?", prod.ProductGroupId).
		Preload("SpecAttributes").
		Find(&specGroup).Error

	return prod, specGroup, err
}

func (s *productService) Query(query string, args []any, orderBy string, page string) ([]model.Product, error) {
	var pageNum, err = strconv.Atoi(page)
	if err != nil {
		pageNum = 1
	}
	var offset = 10 * (pageNum - 1)
	var prod []model.Product
	var dbErr = model.DB.Where(query, args...).
		Order(orderBy).
		Limit(10).Offset(offset).
		Preload("Specs", "is_overview = ?", true).
		Find(&prod).Error

	return prod, dbErr
}

func (s *productService) Update(payload *model.Product) error {
	var err = model.DB.Updates(payload).Error
	return err
}

func (s *productService) Delete(id int) error {
	var err = model.DB.Delete(&model.Product{Id: id}).Error
	return err
}

func (s *productService) CountProduct() ([]dto.ProdCountDto, error) {
	var prod []dto.ProdCountDto
	var err = model.DB.Model(&model.Product{}).
		Select("brand_id, count(id) as total").
		Group("brand_id").
		Preload("Brand").
		Find(&prod).Error

	return prod, err
}

func (s *productService) CountDevice() ([]dto.DeviceCountDto, error) {
	var prod []dto.DeviceCountDto
	var err = model.DB.Table("products").
		Select("products.name, count(devices.id) as total").
		Joins("LEFT JOIN devices ON devices.product_id = products.id AND devices.status <> ?", "sold").
		Group("products.id").
		Find(&prod).Error

	return prod, err
}
