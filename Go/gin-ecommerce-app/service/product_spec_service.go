package service

import (
	"main/model"
)

type specService struct{}

/* query "product_group_id = ?" | "product_type_id = ?"
 */
func (s *specService) GetSpecGroup(query string, id int) ([]model.SpecGroup, error) {
	var specGroup []model.SpecGroup
	var err = model.DB.Preload("SpecAttributes.Specs").Where(query, id).Find(&specGroup).Error
	return specGroup, err
}
