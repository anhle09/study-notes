package handler_test

import (
	"testing"

	"main/common/middleware"
	"main/common/util"
	"main/handler"
	"main/model"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var pgTest = util.TestGroup{
	"create_groups_successful": {
		Method: "POST",
		Url:    "/product-group",
		Token:  "admin",
		Body: []model.ProductGroup{
			{Name: "Mobile"},
			{Name: "Laptop"},
			{Name: "Tablet"},
		},
		Expected: 200,
	},
	"get_groups_successful": {
		Method:   "GET",
		Url:      "/product-group",
		Token:    "admin",
		Expected: 200,
	},
}

func TestProductGroupRoutes(t *testing.T) {
	model.ConnectDatabase()
	model.MigrateDatabase()
	a := assert.New(t)
	e := gin.New()
	e.Use(middleware.ErrorHandler)
	handler.RegisterProdGroupRoutes(e.Group(""))

	util.RunTest(e, a, pgTest["create_groups_successful"], "create_groups_successful")
	util.RunTest(e, a, pgTest["get_groups_successful"], "get_groups_successful")
}
