package handler_test

import (
	"testing"

	"main/common/middleware"
	"main/common/seed"
	"main/common/util"
	"main/handler"
	"main/model"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

var prodTestCases = util.TestGroup{
	"create_prod_successful": {
		Method:   "POST",
		Url:      "/product",
		Token:    "admin",
		Body:     seed.CreateMockProduct(seed.Mobile, seed.Samsung, "Samsung"),
		Expected: 200,
	},
	"get_prod_successful": {
		Method:   "GET",
		Url:      "/product/5",
		Token:    "admin",
		Expected: 200,
	},
	"get_prods_successful": {
		Method:   "GET",
		Url:      "/product",
		Token:    "admin",
		Expected: 200,
	},
}

func TestProductRoutes(t *testing.T) {
	model.ConnectDatabase()
	// model.MigrateDatabase()
	a := assert.New(t)
	e := gin.New()
	e.Use(middleware.ErrorHandler)
	handler.RegisterProdRoutes(e.Group(""))

	// util.RunTest(e, a, prodTestCases["create_prod_successful"], "create_prod_successful")
	util.RunTest(e, a, prodTestCases["get_prod_successful"], "get_prod_successful")
	// util.RunTest(e, a, prodTestCases["get_prods_successful"], "get_prods_successful")
}
