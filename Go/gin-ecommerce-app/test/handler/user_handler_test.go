package handler_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"main/handler"
	"main/model"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

type TestCase struct {
	Method       string
	Url          string
	Token        string
	Body         any
	ExpectedCode int
	ExpectedData any
}

type Response[T any] struct {
	Data T
}

func objectToBuffer(obj any) *bytes.Buffer {
	b, _ := json.Marshal(obj)
	return bytes.NewBuffer(b)
}

func bufferToObject[T any](buf *bytes.Buffer) T {
	var res Response[T]
	json.Unmarshal(buf.Bytes(), &res)
	return res.Data
}

var testCases = map[string]TestCase{
	"create_user_success": {
		Method: "POST",
		Url:    "/user",
		Token:  "",
		Body: model.User{
			Email:    "user01@mail.com",
			Password: "1234",
		},
		ExpectedCode: 200,
	},
	"create_user_unauthorized": {
		Method: "POST",
		Url:    "/user",
		Body: model.User{
			Email:    "user01@mail.com",
			Password: "1234",
		},
		ExpectedCode: 401,
	},
	"get_user_success": {
		Method:       "GET",
		Url:          "/user/1",
		ExpectedCode: 200,
		ExpectedData: model.User{
			Id:       1,
			Email:    "user01@mail.com",
			Password: "12345",
		},
	},
}

func TestUserRoutes(t *testing.T) {
	var asserts = assert.New(t)
	var app = gin.New()
	model.ConnectDatabase()
	handler.RegisterUserRoutes(app.Group(""))

	for key, item := range testCases {
		var body = objectToBuffer(item.Body)
		var req, _ = http.NewRequest(item.Method, item.Url, body)
		req.Header.Set("Content-Type", "application/json")
		req.Header.Set("Authorization", item.Token)

		var res = httptest.NewRecorder()
		app.ServeHTTP(res, req)
		asserts.Equal(item.ExpectedCode, res.Code, key)

		if item.ExpectedData != nil {
			var data = bufferToObject[model.User](res.Body)
			asserts.Equal(item.ExpectedData, data, key)
		}
	}
}

func runTest(app *gin.Engine, asserts *assert.Assertions, testData TestCase) {
	var body = objectToBuffer(testData.Body)
	var req, _ = http.NewRequest(testData.Method, testData.Url, body)
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", testData.Token)

	var res = httptest.NewRecorder()
	app.ServeHTTP(res, req)
	asserts.Equal(testData.ExpectedCode, res.Code)

	if testData.ExpectedData != nil {
		var data = bufferToObject[model.User](res.Body)
		asserts.Equal(testData.ExpectedData, data)
	}
}
