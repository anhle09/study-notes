package model_test

import (
	"main/common/seed"
	"main/model"
	"main/service"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ProductInstanceModel(t *testing.T) {
	model.ConnectDatabase()
	// model.MigrateDatabase()
	var as = assert.New(t)
	var deviceList []model.Device
	var err error

	/* CreateMany */
	deviceList = seed.CreateMockDeviceList(1, 10)
	err = service.Device.CreateMany(&deviceList)
	as.Nil(err)

	/* GetMany */
	// deviceList, err = service.Device.GetMany("store_id = ?", []any{2})
	// as.Greater(deviceList[0].Id, 0)
	// as.Nil(err)

	/* ShipToStore */
	// err = service.Device.ShipToStore(seed.Saigon, []int{1, 2, 3})
	// as.Nil(err)

	/* ShipToClient */
	// err = service.Device.ShipToClient(1, 3)
	// as.Nil(err)
}
