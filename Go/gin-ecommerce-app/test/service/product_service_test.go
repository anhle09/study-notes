package model_test

import (
	"main/model"
	"main/service"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProductQuery(t *testing.T) {
	var asserts = assert.New(t)
	model.ConnectDatabase()

	// product search
	// res, _ := prods.GetMany("name LIKE ? OR sku LIKE ?", []any{"%20%", "%20%"}) // case sensitive
	// product list
	// res, _ := prods.GetMany("product_group_id = ? AND brand_id = ?", []any{1, 2})

	// product filter: rom = 51 & ram = 16 -> (specId = 6) & (specId = 9)
	var prodList1 []int
	var prodList2 []int
	model.DB.Model(model.ProductSpecMapping{}).Select("product_id").Where("spec_id = ?", 6).Find(&prodList1)
	model.DB.Model(model.ProductSpecMapping{}).Select("product_id").Where("spec_id = ?", 9).Find(&prodList2)
	res, _ := service.Product.Query("id IN ? AND id IN ?", []any{prodList1, prodList2}, "name", "1")

	// var prodList1 = []int{1, 3, 5, 7, 9, 10, 11, 12}
	// var prodList2 = []int{2, 4, 6, 8, 10, 11, 12}
	// res, _ := service.Product.Query("id IN ? AND id IN ?", []any{prodList1, prodList2}, "name", 1)
	// res, _ := service.Product.Query("id IN ?", []any{[]int{10, 11, 12}}, "name", 1)

	asserts.NotNil(res)
}

func TestProductDetail(t *testing.T) {
	var asserts = assert.New(t)
	model.ConnectDatabase()
	var prod, specGroup, _ = service.Product.Detail(1)

	asserts.NotNil(prod)
	asserts.NotNil(specGroup)
}

func Test_ProductSummary(t *testing.T) {
	model.ConnectDatabase()
	// model.MigrateDatabase()
	var as = assert.New(t)

	var list, err = service.Product.CountProduct()
	// var list, err = service.Product.CountDevice()
	as.NotNil(list)
	as.Nil(err)
}
