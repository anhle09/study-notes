package model_test

import (
	"main/model"
	"main/service"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetSpecForm(t *testing.T) {
	var as = assert.New(t)
	model.ConnectDatabase()

	var sg, err = service.Spec.GetSpecGroup("product_group_id = ?", 1)
	as.Greater(len(sg), 0)
	as.Nil(err)
}
