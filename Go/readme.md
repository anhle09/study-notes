## boilerplate

# https://github.com/Massad/gin-boilerplate (700)
- mvc structure, gorp mysql, redis

# https://github.com/vsouza/go-gin-boilerplate	(500)
- mvc structure, aws sdk dynamodb, viper

# https://github.com/gothinkster/golang-gin-realworld-example-app (2.1k)
- module structure, gorm sqlite, unit test
