package app

import "fmt"

func ExportedFunc(name string) string {
	var message = fmt.Sprintf(">> Hello %v", name)
	return message
}

var AnonyFunc = func() {
	fmt.Sprintln(">> Anonymous function")
}
