package app

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	var app = gin.Default()

	app.GET("/ping", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "pong")
	})

	app.GET("/param/:name", func(ctx *gin.Context) {
		var paramValue = ctx.Params.ByName("name")
		ctx.JSON(http.StatusOK, gin.H{"param": paramValue})
	})

	app.POST("/post", func(ctx *gin.Context) {
		var payload struct {
			Email    string `json:"email" binding:"required"`
			Password string `json:"password" binding:"required"`
		}
		var err = ctx.Bind(&payload)
		if err == nil {
			ctx.JSON(http.StatusOK, gin.H{"data": payload})
		} else {
			ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		}
	})

	app.Run()
}
