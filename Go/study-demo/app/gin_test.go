package app

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestResponse(t *testing.T) {
	var asserts = assert.New(t)
	var app = gin.New()

	app.GET("/get", func(ctx *gin.Context) {
		// ctx.JSON(200, gin.H{"data": "hello"})
		// ctx.String(200, "hello")
		ctx.Status(200)
	})

	var req, _ = http.NewRequest("GET", "/get", nil)
	req.Header.Set("Content-Type", "application/json")

	var res = httptest.NewRecorder()
	app.ServeHTTP(res, req)
	asserts.Equal(200, res.Code) // pass
}
