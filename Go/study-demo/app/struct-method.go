package app

import "fmt"

type Cat struct {
	Name  string
	Color string
}

// stringer -> require pointer
func (c *Cat) String() string {
	return fmt.Sprintf(">> Struct stringer: %s", c.Name) // "Struct display:" + Name
}

type Fish struct {
	Name string
	Size int
}

// method
func (f *Fish) SayHi() {
	fmt.Println(">> Hi")
}

func StructMethod() {
	var cat = &Cat{Name: "John"} // init struct
	fmt.Println(cat)             // user.String()

	var fish = Fish{Name: "Doe", Size: 50}
	fmt.Println(fish) // {Doe 50}
	fish.SayHi()
}
