package app

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

type User struct {
	Name      string    `json:"name"`
	Email     string    `json:"email"`
	Password  string    `json:"-"`             // private -> omit
	Age       int       `json:"age,omitempty"` // empty -> omit
	Hobby     []string  `json:"hobby"`
	CreatedAt time.Time `json:"createdAt"`
}

func TestStructJson(t *testing.T) {
	var user = User{
		Name:      "john",
		Hobby:     []string{"aa", "bb"},
		CreatedAt: time.Now(),
	}

	// var out, err = json.MarshalIndent(user, "", "  ") // add format
	var out, _ = json.Marshal(user)
	var str = string(out) // []byte -> string
	var res User
	json.Unmarshal(out, &res)

	fmt.Println(str)
	fmt.Println(res)
}

func TestMapJson(t *testing.T) {
	var user = map[string]any{"name": "aa", "age": 4}
	var out, _ = json.Marshal(user)
	var res User
	json.Unmarshal(out, &res) // res = struct ? struct : map
}
