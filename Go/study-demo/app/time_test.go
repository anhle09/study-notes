package app

import (
	"fmt"
	"testing"
	"time"
)

var log = fmt.Println

func delay(second int64) {
	t1 := time.Now().Unix()
	t2 := time.Now().Unix()
	for t2-t1 < second {
		t2 = time.Now().Unix()
	}
}

type service struct{}

func (s *service) hi(msg string) {
	time.Sleep(4 * time.Second)
	// delay(4)
	log(msg)
}

var s service

func f2() {
	s.hi("f2")
}

func f3() {
	s.hi("f3")
}

func Test_Time(t *testing.T) {
	t1 := time.Now().UnixNano()
	time.Sleep(3 * time.Second)
	// for i := 0; i < 20; i++ {
	// 	log("delay")
	// }
	t2 := time.Now().UnixNano()
	log(t1, t2)
}

func Test_Block(t *testing.T) {
	log("start")
	go f2()
	go f3()
	log("end")
	time.Sleep(5 * time.Second)
}
