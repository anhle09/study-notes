package app

import (
	"fmt"
	"math/rand"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type Res struct {
	Code int
	Data string
}

func TestDemo(t *testing.T) {
	var asserts = assert.New(t)
	var ept = Res{Code: 200, Data: "test"}

	asserts.Equal(&ept, &Res{Code: 200, Data: "test"}) // pass
	asserts.Equal(ept, Res{Code: 200, Data: "test"})   // pass
	// asserts.Equal(ept, Res{Code: 400, Data: "test"})   // fail

	// asserts.Equal(ept, map[string]any{"Code": 200, "Data": "test"}) // fail
	// asserts.Equal(map[string]any{"a": 1, "b": "b"}, map[string]any{"a": 1, "b": "b"}) // pass
	asserts.Equal(map[string]any{"a": 1, "b": "b"}, map[string]any{"b": "b", "a": 1}) // pass
}

func random() {
	fmt.Println(rand.Intn(100))
}

func TestTime(t *testing.T) {
	rand.Seed(time.Now().Unix())
	random()
	random()
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func BenchmarkFindMax(b *testing.B) {
	for i := 0; i < b.N; i++ {
		max(1, 2)
		time.Sleep(time.Millisecond * 5)
	}
	fmt.Println(b.N)
}
