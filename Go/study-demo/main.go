package main

import (
	"fmt"
	app "main/app"
)

func main() {
	fmt.Println(">> Api starting...")
	app.InitApi()

	// app.StructMethod()
	// app.StructJson()
}
