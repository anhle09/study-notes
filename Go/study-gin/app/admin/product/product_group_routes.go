package product

import (
	"net/http"

	"main/app/model"

	"github.com/gin-gonic/gin"
)

func RegisterProductGroupRoutes(router *gin.RouterGroup) {
	router.POST("/product-group", createProductGroup)
	router.GET("/product-group", getProductGroups)
	// router.PUT("/product-group", updateMany)
	// router.DELETE("/product-group", deleteOne)
}

func createProductGroup(ctx *gin.Context) {
	var body []model.ProductGroup
	var err = ctx.BindJSON(&body)
	if err != nil {
		ctx.Error(err)
		return
	}
	err = model.DB.Create(&body).Error
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.Status(http.StatusOK)
}

func getProductGroups(ctx *gin.Context) {
	var res []model.ProductGroup
	model.DB.Find(&res)
	ctx.JSON(http.StatusOK, gin.H{"data": res})
}
