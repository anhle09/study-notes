package product

import (
	"net/http"

	"main/app/model"

	"github.com/gin-gonic/gin"
)

func RegisterProductRoutes(router *gin.RouterGroup) {
	router.POST("/product", createProduct)
	router.GET("/product/:id", getProduct)
	router.GET("/product", getProducts)
	router.PUT("/product", updateProduct)
	router.DELETE("/product", deleteProduct)
}

func createProduct(ctx *gin.Context) {
	var prod model.Product
	var err = ctx.BindJSON(&prod)
	if err != nil {
		ctx.Error(err)
		return
	}
	// err = model.DB.Create(&prod).Error
	err = prod.Create()
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": prod.Id})
}

func getProduct(ctx *gin.Context) {
	var prod model.Product
	// model.DB.Find(&prod, ctx.Param("id"))
	var err = prod.GetById(ctx.Param("id"))
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": prod})
}

func getProducts(ctx *gin.Context) {
	var prod model.Product
	// model.DB.Find(&prod)
	var prodList, err = prod.GetMany("")
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.JSON(http.StatusOK, gin.H{"data": prodList})
}

func updateProduct(ctx *gin.Context) {
	var prod model.Product
	var err = ctx.BindJSON(&prod)
	if err != nil {
		ctx.Error(err)
		return
	}
	// err = model.DB.Updates(&prod).Error
	err = prod.Update()
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.Status(http.StatusOK)
}

func deleteProduct(ctx *gin.Context) {
	var prod model.Product
	var err = ctx.BindJSON(&prod)
	if err != nil {
		ctx.Error(err)
		return
	}
	// err = model.DB.Delete(&prod).Error
	err = prod.Delete()
	if err != nil {
		ctx.Error(err)
		return
	}
	ctx.Status(http.StatusOK)
}
