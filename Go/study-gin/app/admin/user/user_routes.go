package user

import (
	"net/http"

	"main/app/model"

	"github.com/gin-gonic/gin"
)

func RegisterRoutes(router *gin.RouterGroup) {
	router.GET("/user", getUsers)
	router.GET("/user/:id", getUser)
	// router.POST("/user", common.AuthMiddleware("admin"), createUser)
	// router.PUT("/user", common.AuthMiddleware("admin"), updateUser)
	// router.DELETE("/user", common.AuthMiddleware("admin"), deleteUser)
}

func createUser(ctx *gin.Context) {
	// add validating payload
	var payload model.User
	var err = ctx.Bind(&payload)
	model.DB.Create(&payload)

	if err == nil {
		ctx.Status(http.StatusOK)
	} else {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}

func getUsers(ctx *gin.Context) {
	var users []model.User
	model.DB.Find(&users)
	ctx.JSON(http.StatusOK, gin.H{"data": users})
}

func getUser(ctx *gin.Context) {
	// add validating param
	var user model.User
	model.DB.First(&user, ctx.Param("id"))
	ctx.JSON(http.StatusOK, gin.H{"data": user})
}

func updateUser(ctx *gin.Context) {
	// add validating payload
	var payload model.User
	var err = ctx.Bind(&payload)
	model.DB.Updates(payload)

	if err == nil {
		ctx.Status(http.StatusOK)
	} else {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}

func deleteUser(ctx *gin.Context) {
	var payload model.User
	var err = ctx.Bind(&payload)
	model.DB.Delete(payload)

	if err == nil {
		ctx.Status(http.StatusOK)
	} else {
		ctx.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
}
