package middleware

import (
	"github.com/gin-gonic/gin"
)

func Authenticate(role string) gin.HandlerFunc {
	return func(ctx *gin.Context) {
		var token = ctx.Request.Header.Get("Authorization")

		if token != role {
			ctx.AbortWithStatus(401)
		}
	}
}
