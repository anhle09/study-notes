package seed

import (
	"testing"
)

func TestSeedDatabase(t *testing.T) {
	SeedDatabase()
}

func TestCreateProduct(t *testing.T) {
	CreateMockProductList(Mobile, Apple, "Apple", 4)
}

func TestCreateInstance(t *testing.T) {
	CreateMockInstanceList(1, 2)
}
