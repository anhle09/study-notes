package main

import (
	"net/http"

	userPkg "main/app/admin/user"
	modelPkg "main/app/model"

	"github.com/gin-gonic/gin"
)

func main() {
	modelPkg.ConnectDatabase()

	var app = gin.Default()
	var router = app.Group("/api")
	userPkg.RegisterRoutes(router)

	app.GET("/ping", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "pong")
	})

	app.Run()
}
