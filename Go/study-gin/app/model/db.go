package model

import (
	"main/app/common/config"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectDatabase() {
	var db, err = gorm.Open(postgres.Open(config.Dsn), &gorm.Config{})

	if err != nil {
		panic("Failed to connect database")
	}
	DB = db
}

func MigrateDatabase() {
	DB.SetupJoinTable(&Product{}, "Specs", &ProductSpecMapping{})
	DB.AutoMigrate(
		&ProductGroup{},
		&Brand{},
		&ProductVariant{},
		&ProductInstance{},
		&Product{},
		&SpecGroup{},
		&SpecAttribute{},
		&Spec{},
		&Store{},
		&Address{},
		// &User{},
	)
}
