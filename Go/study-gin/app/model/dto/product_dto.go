package model

import "main/app/model"

type ProdCountDto struct {
	Total int         `json:"total"`
	Brand model.Brand `json:"brand"`
}

type InventoryStore struct {
	StoreName string `json:"storeName"`
	Total     int    `json:"total"`
}

type ProdSummaryDto struct {
	ProductName    string           `json:"productName"`
	ProductUrl     string           `json:"productUrl"`
	Total          int              `json:"total"`
	InventoryCount int              `json:"inventoryCount"`
	InventoryStore []InventoryStore `json:"inventoryStore"`
}

type ProdInstanceSummaryDto struct {
	VariantName    string `json:"variantName"`
	Total          int    `json:"total"`
	InventoryCount int    `json:"inventoryCount"`
}
