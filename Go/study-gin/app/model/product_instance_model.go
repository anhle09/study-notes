package model

type ProductVariant struct {
	Id        int    `json:"id"`
	ProductId int    `json:"productId"`
	Name      string `json:"name" binding:"required,min=3"`
	Price     int    `json:"price" binding:"required"`
	// Specs     []Spec `json:"specs" binding:"required" gorm:"many2many:ProductSpecMapping;"`
}

type ProductInstance struct {
	Id               int    `json:"id"`
	ProductId        int    `json:"productId"`
	StoreId          int    `json:"storeId,omitempty"`
	ClientId         int    `json:"clientId,omitempty"`
	ProductVariantId int    `json:"productVariantId,omitempty"`
	Serial           string `json:"serial"`
	Status           string `json:"status,omitempty"`
	ActiveAt         string `json:"activeAt,omitempty"`
	// ProductVariant   ProductVariant `json:"productVariant,omitempty"`
}

func (pi *ProductInstance) CreateMany(list *[]ProductInstance) error {
	return DB.Create(list).Error
}

/* query "product_id = ? | store_id = ? | serial LIKE ?"
 */
func (pi *ProductInstance) GetMany(query string, args []any) ([]ProductInstance, error) {
	var list []ProductInstance
	var err = DB.Where(query, args...).Find(&list).Error
	return list, err
}

func (pi *ProductInstance) ShipToStore(storeId int, prodIds []int) error {
	return DB.Where("id IN ?", prodIds).Updates(ProductInstance{StoreId: storeId}).Error
}

func (pi *ProductInstance) ShipToClient(clientId int, prodId int) error {
	return DB.Updates(ProductInstance{Id: prodId, ClientId: clientId}).Error
}
