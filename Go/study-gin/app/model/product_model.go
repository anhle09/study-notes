package model

type Product struct {
	Id             int                  `json:"id"`
	ProductGroupId int                  `json:"productGroupId" binding:"required"`
	ProductTypeId  int                  `json:"productTypeId"`
	BrandId        int                  `json:"brandId" binding:"required"`
	Brand          Brand                `json:"brand"`
	Name           string               `json:"name" binding:"required,min=3"`
	Price          int                  `json:"price" binding:"required"`
	Sku            string               `json:"sku" binding:"required"`
	Variants       []ProductVariant     `json:"variants,omitempty"`
	SpecIds        []ProductSpecMapping `json:"specIds" binding:"required"`
	Specs          []Spec               `json:"specs" gorm:"many2many:ProductSpecMapping;"`
	Status         string               `json:",omitempty"`
	Description    string               `json:",omitempty"`
	Images         string               `json:",omitempty"`
	Discount       string               `json:",omitempty"` // Discount
	Promotions     string               `json:",omitempty"` // []Promotion
	Comments       string               `json:",omitempty"` // []Comment
	CreatedBy      string               `json:",omitempty"` // User
	Rating         string               `json:",omitempty"`
	Slug           string               `json:",omitempty"`
	Seo            string               `json:",omitempty"`
}

type ProductSpecMapping struct {
	ProductId int `gorm:"primaryKey"`
	SpecId    int `gorm:"primaryKey"`
}

func (p *Product) Create() error {
	var err = DB.Create(p).Error
	return err
}

func (p *Product) Detail(id int) ([]SpecGroup, error) {
	var err = DB.Preload("Specs").First(p, id).Error
	if err != nil {
		return []SpecGroup{}, err
	}
	var specGroup []SpecGroup
	err = DB.Where("product_group_id = ?", p.ProductGroupId).Preload("SpecAttributes").Find(&specGroup).Error
	return specGroup, err
}

func (p *Product) Query(query string, args []any, orderBy string, page int) ([]Product, error) {
	var offset = 10 * (page - 1)
	var res []Product
	var err = DB.Where(query, args...).Order(orderBy).Limit(10).Offset(offset).Preload("Specs", "is_summary = ?", true).Find(&res).Error
	return res, err
}

func (p *Product) Update() error {
	var err = DB.Updates(p).Error
	return err
}

func (p *Product) Delete() error {
	var err = DB.Delete(&p).Error
	return err
}

// func (p *Product) CountByBrand() ([]ProdCountDto, error) {
// 	var prod []ProdCountDto
// 	var err = DB.Model(p).Select("count(id) as total").Group("brand_id").Preload("Brand").Find(&prod).Error
// 	return prod, err
// }
