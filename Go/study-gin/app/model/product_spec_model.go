package model

type Spec struct {
	Id              int    `json:"id"`
	SpecGroupId     int    `json:"specGroupId"`
	SpecAttributeId int    `json:"specAttributeId"`
	Value           string `json:"value" binding:"required"`
	IsSummary       bool   `json:"isSummary,omitempty"`
	Icon            string `json:",omitempty"`
}

type SpecAttribute struct {
	Id          int    `json:"id"`
	SpecGroupId int    `json:"specGroupId"`
	Name        string `json:"name"`
	Specs       []Spec `json:"specs"`
}

type SpecGroup struct {
	Id             int             `json:"id"`
	ProductGroupId int             `json:"productGroupId" binding:"required"`
	ProductTypeId  int             `json:"productTypeId"`
	Name           string          `json:"name"`
	SpecAttributes []SpecAttribute `json:"specAttributes"`
}

/**
 * query "product_group_id = ?" | "product_type_id = ?"
 */
func (s *SpecGroup) Query(query string, id int) ([]SpecGroup, error) {
	var sg []SpecGroup
	var err = DB.Preload("SpecAttributes.Specs").Where(query, id).Find(&sg).Error
	return sg, err
}
