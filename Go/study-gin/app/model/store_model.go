package model

type Store struct {
	Id      int     `json:"id"`
	Name    string  `json:"name" gorm:"not null" binding:"required"`
	Address Address `json:"address" gorm:"ForeignKey:OwnerId;"`
}

type Address struct {
	Id       int    `json:"id"`
	OwnerId  int    `json:"ownerId"`
	Zip      int    `json:"zip"`
	Country  string `json:"country"`
	City     string `json:"city"`
	District string `json:"district"`
	Ward     string `json:"ware"`
	Street   string `json:"street"`
}
