package model_test

import (
	"main/app/model"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestQuery(t *testing.T) {
	var asserts = assert.New(t)
	model.ConnectDatabase()
	var prod model.Product

	// prod search
	// res, _ := prods.GetMany("name LIKE ? OR sku LIKE ?", []any{"%20%", "%20%"}) // case sensitive
	// prod list
	// res, _ := prods.GetMany("product_group_id = ? AND brand_id = ?", []any{1, 2})

	// prod filter: rom = 51 & ram = 16 -> (specId = 6) & (specId = 9)
	var prodList1 []int
	var prodList2 []int
	model.DB.Model(model.ProductSpecMapping{}).Select("product_id").Where("spec_id = ?", 6).Find(&prodList1)
	model.DB.Model(model.ProductSpecMapping{}).Select("product_id").Where("spec_id = ?", 9).Find(&prodList2)

	res, _ := prod.Query("id IN ? AND id IN ?", []any{prodList1, prodList2}, "name", 1)
	asserts.NotNil(res)
}

func TestPreload(t *testing.T) {
	model.ConnectDatabase()
	// model.MigrateDatabase()
	var prods []model.Product
	// var specs []model.Spec
	// model.DB.Preload("Specs", "Value = ?", "16").Find(&prods)
	// model.DB.Preload("Specs", "Value IN (?)", "16").Find(&prods)
	// model.DB.Preload("Specs").Find(&prods, "Sku = ? OR Sku = ?", 7700, 7796)
	// model.DB.Preload("Products.Specs").Find(&specs, "Value = ? OR Value = ?", "4", "16")
	// model.DB.Preload("Specs").Find(&prods, "specs.value = ?", "4")
	model.DB.Preload("Specs").Find(&prods)
}

func TestAssociation(t *testing.T) {
	model.ConnectDatabase()
	var specs []model.Spec
	model.DB.Model(model.Product{Id: 1}).Association("Specs").Find(&specs)
}

func TestJoin(t *testing.T) {
	model.ConnectDatabase()
	var prods []model.Product
	model.DB.Joins("JOIN product_spec ON product_spec.product_id = products.id").Find(&prods)
}
