package model_test

import (
	"main/app/model"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_ProductInstanceModel(t *testing.T) {
	model.ConnectDatabase()
	model.MigrateDatabase()
	var as = assert.New(t)
	var pi model.ProductInstance
	var piList []model.ProductInstance
	var err error

	/* CreateMany */
	// piList = seed.CreateMockInstanceList(1, 10)
	// err = pi.CreateMany(&list)
	// as.Nil(err)

	/* GetMany */
	piList, err = pi.GetMany("store_id = ?", []any{2})
	as.Greater(piList[0].Id, 0)
	as.Nil(err)

	/* ShipToStore */
	// err = pi.ShipToStore(seed.Saigon, []int{1, 2, 3})
	// as.Nil(err)

	/* ShipToClient */
	// err = pi.ShipToClient(1, 3)
	// as.Nil(err)
}
