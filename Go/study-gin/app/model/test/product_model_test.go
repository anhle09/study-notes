package model_test

import (
	"main/app/model"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestProductQuery(t *testing.T) {
	var asserts = assert.New(t)
	model.ConnectDatabase()
	var prod model.Product

	// prod search
	// res, _ := prods.GetMany("name LIKE ? OR sku LIKE ?", []any{"%20%", "%20%"}) // case sensitive
	// prod list
	// res, _ := prods.GetMany("product_group_id = ? AND brand_id = ?", []any{1, 2})

	// prod filter: rom = 51 & ram = 16 -> (specId = 6) & (specId = 9)
	var prodList1 []int
	var prodList2 []int
	model.DB.Model(model.ProductSpecMapping{}).Select("product_id").Where("spec_id = ?", 6).Find(&prodList1)
	model.DB.Model(model.ProductSpecMapping{}).Select("product_id").Where("spec_id = ?", 9).Find(&prodList2)

	res, _ := prod.Query("id IN ? AND id IN ?", []any{prodList1, prodList2}, "name", 1)
	asserts.NotNil(res)
}

func TestProductDetail(t *testing.T) {
	var asserts = assert.New(t)
	model.ConnectDatabase()
	var prod model.Product
	var specGroup, _ = prod.Detail(1)

	asserts.NotNil(prod)
	asserts.NotNil(specGroup)
}

func Test_ProductSummary(t *testing.T) {
	model.ConnectDatabase()
	model.MigrateDatabase()
	var as = assert.New(t)
	var prod model.Product

	var list, err = prod.CountByBrand()
	as.NotNil(list)
	as.Nil(err)
}
