package model_test

import (
	"main/app/model"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetSpecForm(t *testing.T) {
	var as = assert.New(t)
	model.ConnectDatabase()

	var specGroup model.SpecGroup
	var sg, err = specGroup.Query("product_group_id = ?", 1)
	as.Greater(len(sg), 0)
	as.Nil(err)
}
