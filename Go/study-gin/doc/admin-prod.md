# OFFICE

## create prod page

> seed prod group, brand, spec
> get spec form (group, attribute, value) by prodGroupId, prodTypeId
> create product (coming soon)
> create instance
> update instance -> ship to store -> release 
> update instance -> ship to customer -> sold
> update instance -> return, check

# STORE

> search product inventory: by name, serial

# dashboard (group by prod group)

- count prod by brand -> []{brandName, count}
- list prod by brand -> []{prodName, total count, inventoryCount} -> get prod by brand, count instance by prod
- instance by prod -> []instance, []{variantName, inventoryCount} -> get instance by prod
