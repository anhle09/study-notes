## json

- json.Marshal(obj) -> bytes
- json.Unmarshal(bytes, &struct) -> not &any (-> res.data...)

## binding

- ctx.Bind(&payload) -> case-insensitive
- ctx.JSON(200, gin.H{"data": data}) -> case-sensitive
-> map by json tag name

## product client api (public): mobile ex

> get mb list (pagination): thumbnail, name, spec (some), variants
> filter: brand, price, feature, pin, display, camera
> sort: popular, price, discount
> get mb detail + spec group + attribute
- get store inventory

## li do co spec model

- tranh trung lap
- de render ui (group)
- de them spec item (tinh nang cua sp)
- moi sp co spec khac nhau
