const tableData = [
  {
    id: 0,
    image: 'image/dsrl-camera.jpg',
    name: 'MASSA AST',
    description: 'Color: black, Material: metal',
    quantity: 1,
    price: 120,
    discount: 25,
  },
  {
    id: 1,
    image: 'image/sd-card.jpg',
    name: 'MASSA AST',
    description: 'Color: black, Material: metal',
    quantity: 1,
    price: 7,
    discount: 0,
  },
  {
    id: 2,
    image: 'image/compact-camera.jpg',
    name: 'MASSA AST',
    description: 'Color: black, Material: metal',
    quantity: 1,
    price: 120,
    discount: 25,
  },
];

function calculateItemTax(item) {
  const total = item.quantity * item.price * 12.5 / 100;
  return Math.round(total);
}

function calculateItemTotal(item) {
  return item.quantity * item.price - item.discount + calculateItemTax(item);
}

function calculateTotalPrice() {
  let total = 0;
  for (let item of tableData) {
    total += calculateItemTotal(item);
  }
  return total;
}

function calculateTotalDiscount() {
  let total = 0;
  for (let item of tableData) {
    total += item.discount;
  }
  return total;
}

function calculateTotalTax() {
  let total = 0;
  for (let item of tableData) {
    total += calculateItemTax(item);
  }
  return total;
}

const renderTableRow = (data, index) => `
  <tr>
    <td><img src="${data.image}" alt="${data.image}"></td>
    <td>${data.name}<br>${data.description}</td>
    <td>
      <span class="box-quantity">${data.quantity}</span>
      <button class="btn-order"
        onclick="handleMinus(${index})"
      >-</button>
      <button class="btn-order"
        onclick="handlePlus(${index})"
      >+</button>
      <button class="btn-remove"
        onclick="handleRemove(${index})"
      >×</button>
    </td>
    <td>$${data.price}.00</td>
    <td>${data.discount ? '$'+data.discount+'.00' : '--'}</td>
    <td>$${calculateItemTax(data)}.00</td>
    <td>$${calculateItemTotal(data)}.00</td>
  </tr>
`

function renderTotalOrder() {
  document.getElementById('totalPrice').innerText = `$${calculateTotalPrice()}.00`;
  document.getElementById('totalDiscount').innerText = `$${calculateTotalDiscount()}.00`;
  document.getElementById('totalTax').innerText = `$${calculateTotalTax()}.00`;
}

function renderTableOrder() {
  let tableBody = '';
  tableData.forEach((item, index) => {
    tableBody += renderTableRow(item, index);
  });
  document.getElementById('tableBody').innerHTML = tableBody;
}

function render() {
  renderTableOrder();
  renderTotalOrder();
}

const handleMinus = (index) => {
  if (tableData[index].quantity > 1) {
    tableData[index].quantity--;
  }
  render();
}

const handlePlus = (index) => {
  tableData[index].quantity++;
  render();
}

const handleRemove = (index) => {
  tableData.splice(index, 1);
  render();
}

render();

/* const handleButtonClick = (action, id) => {
  tableData.forEach((item, index) => {
    if (item.id === id) {
      if (action === 'plus') {
        tableData[index].quantity++;
      }
      else if (action === 'remove') {
        tableData.splice(index, 1);
      }
      else if (tableData[index].quantity > 1) {
        tableData[index].quantity--;
      }
    }
  })
  render();
} */
