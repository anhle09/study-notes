if ('serviceWorker' in navigator) {

    navigator.serviceWorker.register('service-worker.js')
        // Wait until the service worker is active.
        .then(() => navigator.serviceWorker.ready)
        // then show the interface for the commands once it's ready.
        .then(() => {
            console.log('>> sw registered')
            // First we initialize the channel
            /* const messageChannel = new MessageChannel();
            navigator.serviceWorker.controller?.postMessage({
                payload: 'hello from web',
            }, [messageChannel.port2])

            messageChannel.port1.onmessage = (event) => {
                console.log('>> onmessage:', event.data);
            } */
        })
        .catch((error) => {
            // Something went wrong during registration
            console.log(error)
        })
} else {
    console.log('Service workers is not support')
}

document.querySelector('#get').onclick = () => {
    fetch('https://jsonplaceholder.typicode.com/todos/1')
}

document.querySelector('#post').onclick = () => {
    fetch('https://jsonplaceholder.typicode.com/todos', {
        method: 'POST',
        body: JSON.stringify({ name: 'test' }),
        'content-type': 'application/json',
    })
}
