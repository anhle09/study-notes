const data = []
const baseUrl = 'https://jsonplaceholder.typicode.com'

self.addEventListener('activate', (event) => {
    console.log('>> sw activated')
})

/* self.addEventListener('message', (event) => {
    const handler = new Promise(resolve => {
        console.log('>> postMessage:', event.data)
        event.ports[0].postMessage({ status: 'ok' })
    })

    // the waitUntil() method for extending the lifetime until the promise is resolved.
    if ('waitUntil' in event) {
        event.waitUntil(handler)
    }

    // Without support for waitUntil(), there's a chance that if the promise chain
    // takes "too long" to execute, the service worker might be automatically
    // stopped before it's complete.
}) */

self.addEventListener("fetch", (event) => {
    const req = event.request

    if (req.url.includes(baseUrl) && req.method == 'GET') {
        fetch(event.request)
            .then(res => res.json())
            .then(body => {
                const schema = {}
                Object.keys(body).forEach(key => schema[key] = typeof body[key])
                data.push({ method: req.method, url: req.url, body: schema })
                console.log(data)
            })
        event.respondWith(fetch(event.request))
    }
    else if (req.url.includes(baseUrl)) {   // !== 'GET'
        // Prevent the default, and handle the request ourselves.
        // event.respondWith(fetch(event.request))
        event.respondWith(null)
        req.json().then(body => {
            const schema = {}
            Object.keys(body).forEach(key => schema[key] = typeof body[key])
            data.push({ method: req.method, url: req.url, body: schema })
            console.log(data)
        })
        // event.respondWith(fetch('ping'))    // 404
    }

    // Let the browser do its default thing
})
