if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('sw.js')
        .then(() => navigator.serviceWorker.ready)
        .then(() => {
            console.log('>> sw registered')
        })
        .catch((error) => {
            console.log(error)
        })
}
