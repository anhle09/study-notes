const apiSchema = {}
const baseUrl = 'localhost:8081/api'

self.addEventListener('activate', (event) => {
    console.log('>> sw activated')
})

self.addEventListener("fetch", (event) => {
    const req = event.request

    if (req.url.includes('notifications')) return

    if (req.url.includes(baseUrl) && req.method == 'GET') {
        fetch(event.request)
            .then(res => res.json())
            .then(body => {
                // apiSchema.push({ method: req.method, url: req.url, body: getSchema(body) })
                apiSchema[req.url] = { method: req.method, body: getSchema(body) }
                console.log(apiSchema)
            })
        event.respondWith(fetch(event.request))
    }
    else if (req.url.includes(baseUrl)) {   // !== 'GET'
        req.json().then(body => {
            // apiSchema.push({ method: req.method, url: req.url, body: getSchema(body) })
            apiSchema[req.url] = { method: req.method, body: getSchema(body) }
            console.log(apiSchema)
        })
        event.respondWith(fetch('404'))    // 404
    }
})

function getSchema(data) {
    if (data === null) return null
    if (Array.isArray(data)) return [getSchema(data[0])]
    if (typeof data !== 'object') return typeof data

    const schema = {}
    Object.keys(data).forEach(key => {
        schema[key] = getSchema(data[key])
    })
    return schema
}
