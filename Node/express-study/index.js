const express = require('express')
const app = express()

// require('dotenv').config();
const port = process.env.PORT || 3000;

app.get('/', async (req, res) => {
  console.log((new Date()).toISOString(), 'start')
  await new Promise(res => setTimeout(res, 5000))
  res.send('Hello');
  console.log((new Date()).toISOString(), 'end')
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
