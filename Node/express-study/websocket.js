// server.js
const express = require('express');
const WebSocket = require('ws');
const http = require('http');

const app = express();

const cors = require('cors')
app.use(cors())

// Create an HTTP server using Express
const server = http.createServer(app);

// Create a WebSocket server on top of the HTTP server
const wss = new WebSocket.Server({ server });

// WebSocket connection handler
wss.on('connection', (ws) => {
  console.log('New WebSocket connection established');

  // Sending a message to the connected client
  ws.send('Welcome to the WebSocket server!');

  // Listen for messages from the client
  ws.on('message', (message) => {
    console.log('Received:', message);
    // Send a message back to the client
    ws.send(`Server received: ${message}`);
  });

  // Handle the connection close event
  ws.on('close', () => {
    console.log('Client disconnected');
  });
});

// Example of an HTTP route
app.get('/', (req, res) => {
  res.send('<h1>Welcome to the WebSocket Example</h1>');
});

app.get('/api', async (req, res) => {
  res.send({ msg: 'api' });
})

// Start the HTTP server (and the WebSocket server)
const PORT = process.env.PORT || 4000;

server.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
