import { Router } from 'express';
import User from '../model/user.model';
import authenticate from '../config/auth';

const router = Router();

router.get('/:id', (req, res, next) => {
  User.findById(req.params.id)
    .then(result => {
      res.json({ user: result });
    })
    .catch(next);
});

router.post('/', (req, res, next) => {
  const data = {
    name: req.body.username,
    email: req.body.email,
  }
  User.create(data, (error, result) => {
    next(error);
    //console.log(error, result);
  })
});

router.post('/login', (req, res, next) => {
  if (req.body.email && req.body.password) {
    authenticate(req.body.email, req.body.password, (error, token) => {
      if (token) {
        res.send(token);
      }
      else {
        res.status(401);
        next(new Error('Wrong email or password'))
      }
    })
  }
  else {
    res.status(400);
    next(new Error('Empty email or password'));;
  }
})

/* router.post('/register', function (req, res, next) {
  if (req.body.password !== req.body.passwordConf) {
    var err = new Error('Passwords do not match.');
    err.status = 400;
    return next(err);
  }

  if (req.body.email &&
    req.body.username &&
    req.body.password &&
    req.body.passwordConf) {

    bcrypt.hash(req.body.password, 10, function (err, hash) {
      if (err) {
        return next(err);
      }

      var userData = {
        email: req.body.email,
        username: req.body.username,
        password: hash,
      }

      User.create(userData, function (error, user) {
        if (error) {
          return next(error);
        } else {
          req.session.userId = user._id;
          return res.redirect('/profile');
        }
      });
    })
  }
  else {
    var err = new Error('All fields required.');
    err.status = 400;
    return next(err);
  }
});

router.get('/', (req, res) => {
  res.send('Please enter user id');
}) */

export default router;