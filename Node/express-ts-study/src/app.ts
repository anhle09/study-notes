import express from 'express';
import mongoose from 'mongoose';
import userApi from './api/user';

var app = express();

mongoose.connect('mongodb://localhost:27017/mongoose-demo')
  .then(() => console.log('MongoDb connected'))
  .catch(err => console.log(err));

app.use(express.json());
app.use(express.urlencoded());

app.get('/', (req, res) => {
  res.send('Hello');
});

app.use('/api/user', userApi);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  var err = new Error('Not Found');
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  res.status(500);
  res.send(err.message);
});

app.listen(5000, () => console.log('Server started on port 5000'));
