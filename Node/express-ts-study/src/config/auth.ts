import User from '../model/user.model';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';

const getToken = (id: string) => {
  return jwt.sign({ id: id }, 'secretkey', { expiresIn: '1h' });
}

const authenticate = (email, password, callback) => {
  User.findOne({ email: email })
    .then(user => {
      if (!user) {
        const error = new Error('User not found');
        callback(error, null);
      }
      else {
        bcrypt.compare(password, user.password, (error, match) => {
          if (error) {
            callback(error, null);
          }
          else if (match) {
            const data = { id: user._id };
            return jwt.sign(data, 'secretkey', { expiresIn: '30s' });
          }
          else {
            const error = new Error('Wrong password');
            callback(error, null);
          }
        })
      }
    })
    .catch((error) => {
      callback(error, null);
    })
}

export default authenticate;
