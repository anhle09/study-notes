import mongoose from 'mongoose';

const articleSchema = new mongoose.Schema({
  slug: String,
  title: String,
  description: String,
  body: String,
  like: { type: Number, default: 0 },
  comment: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }],
  tag: [{ type: String }],
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
}, { timestamps: true });

const articleCollection = mongoose.model('Article', articleSchema);

export default articleCollection;
