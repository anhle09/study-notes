.container {
  display: grid;
  grid-template-columns: 200px 200px 200px;	// 3 col 200px
  grid-template-rows: 100px 100px;		// 2 row 100px
  grid-gap: 20px;
}

grid-template-columns: repeat(3, 200px);
grid-template-rows: repeat(2, 100px);
-> repeat(number, value)

grid-template-columns: repeat(3, 1fr);	// 3 col bang nhau

grid-template-columns: 40% 1fr 2fr;		// 3 col (40 20 40)
-> 1fr la 1 phan cua khong gian trong con lai

.item5 {
  grid-column: 1 / span 3;	// chiem 3 col tu line 1
}

.item1 {
  grid-column: 1 / 4;	// tu line 1 -> 4 = 3 col
  grid-column: 1 / -1;	// tu line 1 -> -1 = all col
  grid-row: 1 / 3;		// tu line 1 -> 3 = 2 row
}

grid-template-rows: minmax(80px, auto);	// ??
grid-template-columns: minmax(auto, 10%) 1fr 2fr;	// ??
-> minmax(min, max) -> reponsive (hoac ket hop voi media)

grid-template-columns: repeat(auto-fit, minmax(200px,1fr));
-> fr la tuong doi, max 1fr = 2*min