const loopPromise = () => {
  return new Promise(resolve => {
    setTimeout(() => resolve('ok'), 1000);
  })
}
/* 
loopPromise().then(sum => console.log('Promise.then'));
console.log('hello');
 */
const asyncFunc = async () => {
  let a = await loopPromise();
  console.log('loop 1');
  let b = await loopPromise();
  throw 'error test';
  let c = await loopPromise();
  console.log('loop 3');
  return a + b + c;
}

console.log('before');

/* asyncFunc().then(total => {
  console.log('asyncFunc.then');
}).catch(err => console.log('error:', err)); */

console.log('after');

/* 
#output asyncFunc().then
before
after
loop 1
loop 3
asyncFunc.then

#output asyncFunc().catch
before
after
loop 1
error: error test
 */

function rejectPromise() {
  return new Promise((resolve, reject) => {
    setTimeout(() => reject(new Error('error')), 1000);
  })
}

async function awaitReject() {
  try {
    const a = await rejectPromise();
    console.log('> reject: ', a);
  }
  catch(err) {
    console.log('>', err);
  }
}

awaitReject();