'use strict'

function func() {
  console.log('hi');
}

function func(a, b) {
  console.log(a, b);
}

func();
func(5);
func(5, 7);

/* 
#output:
undefined undefined
5 undefined
5 7
 */