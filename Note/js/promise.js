const testPromise = () => {
  return new Promise(resolve => {
    console.log('hello');
    resolve('resolve');
  })
}

testPromise().then(result => console.log(result));
console.log('hi');

const func = async () => {
  //return "func";
  return console.log('func');
}

const funcCall = async () => {
  //console.log(await func());
  console.log(func());
}

//funcCall();
func();
console.log('funCall');

/*
output:
hello
hi
resolve
 */

async function asyncPromise() {
  if ('error') {
    return Promise.reject('msg')
  }
  return Promise.resolve('data')
}
