/**
 * this tro toi object goi ham do
 * bien, ham global thuoc object window
 * */

var person = {
  firstName: 'Hoang',
  lastName: 'Pham',
  friends : ['Minh', 'Sang', 'Khoa', 'Hoang'],
  showFriend: function() {
    var personObj = this;
    this.friends.forEach(function(fr) {
      console.log(personObj.firstName + ' have a friend named ' + fr);
    });
    /**
     * function(fr) ko phai do object person call
     * object person chi call function() -> personObj = this
     */
  },
  showName: function() {
    console.log(this);
    console.log(this.firstName + ' ' + this.lastName);
    // console.log(firstName + ' ' + lastName);  // error
  },
  showNameArrow: () => {
    console.log(this);
    console.log(this.firstName + ' ' + this.lastName);  // ko dung this trong arrow function
  }
};

person.showName(); //Hoang Pham.
// const func = person.showName.bind(person);  // khi call func() thi do object windwow call

person.showNameArrow(); //undefined
const func = person.showNameArrow.bind(person); // arrow function ko bind this dc
func();   //undefined
