function testPromise(test) {
  return new Promise((resolve, reject) => {
    const callback = () => {
      if (test == 'resolve') {
        resolve('resolve');
      }
      else if (test == 'reject') {
        reject(new Error('reject'));
      }
    }
    setTimeout(callback, 1000);
  })
}

async function tryCatch() {
  try {
    const data = await testPromise('reject');
    console.log('> try', data);
    return { data };
  }
  catch (error) {
    console.log('> catch', error);
    return error;
  }
  console.log('> test return');
}

async function noCatch() {
  const data = await testPromise('reject');
  console.log('>', data);
  return { data };
}

function thenCatch() {
  noCatchError()
    .then(function (result) {
      return result
    })
    .catch(function (error) {
      return error
    });
}

async function test() {
  const data = await tryCatch();
  console.log('> return ', data);
  //const data = await noCatch();  // ko handle error dc

  //console.log('> thenCatch:', thenCatch());
}

test();