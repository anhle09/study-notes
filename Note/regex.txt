** Regular expressions: /pattern/modifiers

1. modifiers

i : ko phan biet chữ hoa chữ thường
g : so khop toàn bộ chuỗi cần tìm

[abc]		Tìm các chữ cái a,b hoặc c
[0-9], [a-z]	Tìm "mot" ký tự từ 0-9 hoặc từ a-z
[0-9]+		Tim "nhieu" chu so 0-9
[0-9]*		Tim co hoac ko chu so 0-9
\d		Tìm ký tự là chữ số

---------------------------------------------------------
2. ex

/^pattern$/		so sanh toan bo chuỗi tu ký tự bắt đầu -> kết thúc
/^[0-9]{8}$/		chữ số [0-9] và chiều dài 8
/^[0-9]{8,}$/		chữ số [0-9] và chiều dài >= 8
/[0-9]/		chua 1 so [0-9]

/^[a-z0-9]+$/i		letter + number
/^[a-z0-9!@#$%&*_?\-]+$/i	letter + number + symbol

-----------------------------------------------
3. test

/^[0-9]{8}$/.test('1234'): tat ca phai la chu so, length = 8 -> return false
/[0-9]/.test('1234'): chi can co 1 chu so -> return true

------------------------------------------
4. match
'abc d'.match(/ab/g) -> return 'ab'
'abc d'.match(/ab/) -> return ''
