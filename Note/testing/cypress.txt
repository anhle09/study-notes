cy.get('.todo-list li').should('have.length', 2)

cy.get()...: first(), last(), type(), parent()
cy.contains('...').click()
cy.url().should('include', '/profile')

should('...'): 'have.length', 'have.text', 'not.exist', 'not.have.text', 'not.be.empty', 'be.visible'
get('...'): '[data-test=name]', '.class tag', 'input[type=checkbox]'

cy.await('1000')

cy.request('POST', '/test/seed/user', { username: 'jane.lane' })
      .its('body')
      .as('currentUser')

'be.visible': width || height != 0
'be.empty': text content != 0, width = any

cy.get('@handleSubmit').should('be.calledWith', payload)  // deep compare payload

