'use strict'

const autocannon = require('autocannon')
const url = 'http://localhost:8080/api/product-group'
// const url = 'http://localhost:5000/api/release'

autocannon({
  url,
  connections: 100,
  pipelining: 1, // default
  duration: 10 // default
}, console.log)
