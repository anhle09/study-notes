"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const database_1 = require("./database");
const env_1 = require("./env");
const app = (0, express_1.default)();
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.use(express_1.default.urlencoded());
app.get('/ping', (req, res) => {
    res.send('pong');
});
app.get('/user', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const result = yield database_1.userRepo.find();
    res.json({ data: result });
}));
// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(new Error('Not Found'));
});
// error handler
app.use((err, req, res, next) => {
    res.status(500).send({ error: err.message });
});
app.listen(env_1.SERVER_PORT, () => console.log(`>> Server started on port ${env_1.SERVER_PORT}`));
database_1.database.createConnection();
//# sourceMappingURL=app.js.map