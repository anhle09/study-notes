"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRepo = exports.database = void 0;
require("reflect-metadata");
const typeorm_1 = require("typeorm");
const env_1 = require("./env");
const entity_1 = require("./entity");
class Database {
    constructor() {
        this.dataSource = new typeorm_1.DataSource({
            host: env_1.DB_CONFIG.HOST,
            port: parseInt(env_1.DB_CONFIG.PORT),
            username: env_1.DB_CONFIG.USERNAME,
            password: env_1.DB_CONFIG.PASSWORD,
            database: env_1.DB_CONFIG.DATABASE,
            type: 'postgres',
            synchronize: true,
            logging: false,
            entities: [entity_1.User],
            migrations: [],
            subscribers: [],
        });
        this.queryRunner = this.dataSource.createQueryRunner();
    }
    get userRepo() {
        return this.queryRunner.manager.getRepository(entity_1.User);
    }
    createConnection() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.dataSource.initialize();
                console.log('>> Database is connected');
            }
            catch (err) {
                console.log('>> Database error', err);
            }
        });
    }
    destroyConnection() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.dataSource.destroy();
        });
    }
}
exports.database = new Database();
exports.userRepo = exports.database.userRepo;
//# sourceMappingURL=database.js.map