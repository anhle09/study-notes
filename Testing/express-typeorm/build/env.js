"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SERVER_PORT = exports.DB_CONFIG = void 0;
require("dotenv/config");
exports.DB_CONFIG = {
    HOST: process.env.DB_HOST,
    PORT: process.env.DB_PORT,
    USERNAME: process.env.DB_USERNAME,
    PASSWORD: process.env.DB_PASSWORD,
    DATABASE: process.env.DB_DATABASE,
};
exports.SERVER_PORT = process.env.SERVER_PORT || '5000';
//# sourceMappingURL=env.js.map