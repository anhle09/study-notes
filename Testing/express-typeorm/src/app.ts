import express from 'express'
import cors from 'cors'
import { database, userRepo } from './database'
import { SERVER_PORT } from './env'
import { fetch, getMsg } from './service'

const app = express()
app.use(cors())
app.use(express.json())
app.use(express.urlencoded())

app.get('/ping', (req, res) => {
    res.send('pong')
})

app.get('/hi', async (req, res) => {
    console.log((new Date()).toISOString(), '/hi')
    res.send(await getMsg())
    console.log((new Date()).toISOString(), '/hi end')
})

app.get('/fetch', async (req, res) => {
    console.log((new Date()).toISOString(), '/fetch')
    const result = await fetch()
    res.send(result.data)
    console.log((new Date()).toISOString(), '/fetch end')
})

app.get('/user', async (req, res) => {
    const result = await userRepo.find()
    res.json({ data: result })
})

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(new Error('Not Found'))
})

// error handler
app.use((err, req, res, next) => {
    res.status(500).send({ error: err.message })
})

app.listen(SERVER_PORT, () => console.log(`>> Server started on port ${SERVER_PORT}`))

database.createConnection()
