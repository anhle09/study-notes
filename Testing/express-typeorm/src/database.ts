import 'reflect-metadata'
import { DataSource, QueryRunner } from 'typeorm'
import { DB_CONFIG } from './env'
import { User } from './entity'

class Database {
    constructor() {
        this.dataSource = new DataSource({
            host: DB_CONFIG.HOST,
            port: parseInt(DB_CONFIG.PORT),
            username: DB_CONFIG.USERNAME,
            password: DB_CONFIG.PASSWORD,
            database: DB_CONFIG.DATABASE,
            type: 'postgres',
            synchronize: true,
            logging: false,
            entities: [User],
            migrations: [],
            subscribers: [],
        })
        this.queryRunner = this.dataSource.createQueryRunner()
    }

    private dataSource: DataSource
    private queryRunner: QueryRunner

    get userRepo() {
        return this.queryRunner.manager.getRepository(User)
    }

    async createConnection() {
        try {
            await this.dataSource.initialize()
            console.log('>> Database is connected')
        } catch (err) {
            console.log('>> Database error', err)
        }
    }

    async destroyConnection() {
        await this.dataSource.destroy()
    }
}

export const database = new Database()
export const userRepo = database.userRepo
