import 'dotenv/config'

export const DB_CONFIG = {
    HOST: process.env.DB_HOST,
    PORT: process.env.DB_PORT,
    USERNAME: process.env.DB_USERNAME,
    PASSWORD: process.env.DB_PASSWORD,
    DATABASE: process.env.DB_DATABASE,
}

export const SERVER_PORT = process.env.SERVER_PORT || '5000'
