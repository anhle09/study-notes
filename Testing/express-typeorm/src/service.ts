import axios from 'axios'

function delay(ms) {
    let t1 = Date.now()
    let t2 = Date.now()
    while (t2 - t1 < ms) {
        t2 = Date.now()
    }
}

export function getMsg() {
    // console.log((new Date()).toISOString(), 'getMsg()')
    return new Promise((res, rej) => {
        delay(5000)
        res('hello')
    })
}

export function fetch() {
    return axios.get('http://localhost:3000')
}
