package main

import (
	"testing"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Product struct {
	Id      int
	BrandId int
	Brand   Brand
	Name    string
	Price   int
}

type Brand struct {
	Id   int
	Name string
}

var DB *gorm.DB

func InitDb() {
	var dsn = "host=localhost user=postgres password=anhle09@Quantic dbname=gorm-test port=5500 sslmode=disable"
	var db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("failed to connect database")
	}
	db.AutoMigrate(&Product{}, &Brand{})
	DB = db

}

func Test_Belong(t *testing.T) {
	InitDb()
	// DB.Create(&Brand{Name: "test"})
	// DB.Create(&Product{BrandId: 2, Name: "test"})
	var p []Product
	DB.Preload("Brand").Find(&p)

	// Create
	// var payload = Product{Code: "D4", Price: 100}
	// db.Create(&payload)

	// Read
	// var product Product
	// db.First(&product, 1) // find product with integer primary key
	// db.First(&product, "code = ?", "D2") // find product with code D42

	// db.Model(Product{Id: 3}).Update("Price", 200)
	// db.Model(Product{Id: 3}).Updates(Product{Price: 50, Code: "F1"}) // non-zero fields

	// Delete - delete product
	// db.Delete(Product{Id: 7})
}
