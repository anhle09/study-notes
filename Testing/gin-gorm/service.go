package main

import (
	"io"
	"net/http"
	"time"
)

func Delay(second int64) {
	t1 := time.Now().Unix()
	t2 := time.Now().Unix()
	for t2-t1 < second {
		t2 = time.Now().Unix()
	}
}

type service struct{}

var Service service

func (s *service) GetMsg() string {
	// time.Sleep(5 * time.Second)
	Delay(5)
	return "hello"
}

func Fetch() string {
	res, err := http.Get("http://localhost:3000")
	if err != nil {
		return ""
	}
	body, err := io.ReadAll(res.Body)
	if err != nil {
		return ""
	}
	return string(body)
}
