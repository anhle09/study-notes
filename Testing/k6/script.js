import http from 'k6/http'
import { sleep } from 'k6'

export const options = {
  vus: 2000,
  duration: '5s',
}

export default function () {
  // http.get('http://172.24.70.214:8080/api/products?orderBy=name')
  http.get('http://localhost:8080/ping')
  // http.get('http://localhost:5000/hi')
  // sleep(1)
}

// docker run --rm -i grafana/k6 run - <script.js
// k6 run script.js

// 1 vus gui req tuan tu, chung 1 http connection, gin block cac req chung 1 connection
// cac vus gui req song song, khac http connection
