function formatComma(num: string | number) {
    let result = '', count = 0;
    const [beforeComma, afterComma] = String(num).split('.');
    for (let i = beforeComma.length - 1; i >= 0; i--) {
        if (count < 3) {
            result = beforeComma[i] + result;
            count++;
        } else {
            result = beforeComma[i] + ',' + result;
            count = 0;
        }
    }
    if (afterComma) {
        count = 0;
        for (let i = 0; i < afterComma.length; i++) {
            if (count < 3) {
                result += afterComma[i];
                count++;
            } else {
                result += ',' + afterComma[i];
                count = 0;
            }
        }
    }
}

console.log(formatComma(1111111111.1111111));
