import { sorUserById } from 'demo-yarn-link'

export class SayHello {
  hello() {
    console.log('helu')
  }
}

export class SayHi extends SayHello {
  sayHi() {
    console.log('hii')
  }
}

const test = new SayHi()
test.sayHi()

console.log(sorUserById([{ id: 1, name: 'A' }]))
