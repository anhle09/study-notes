import 'dotenv/config'
import { sortBy } from 'lodash'

type User = {
  id: number,
  name: string,
}

export const sorUserById = (userList: User[]) => {
  return sortBy(userList, item => item.id)
}

console.log(process.env.MESSAGE)
