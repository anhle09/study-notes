** Course Project (FPT Software Academy) | 09/2021 - 05/2022 
- Business: quan ly task, user va file
- Stack: react, redux, taildwind, express, mongodb
- Role: fullstack

** Devices management system (Vix Technology - Australia) | 06/2022 - 11/2022
- Business: quan ly (quantity, type) san pham, phan mem (repo, version, key), phan phoi, chi nhanh, delivery, rollback
- Stack: react, antd, mobx, scss, nest, postgre, typeorm, jira
- Role: fullstack (crud software repo: release status)

** Eqsurv Manager (Toshiba - Japan) | 12/2022 - 10/2023
- Business: quan ly cong viec (kiem tra bao tri), lich bieu cua nhan vien nha may (xem, thuc hien, submit kq), import/export excel
- Stack: vue, java spring boot, sql, postgre
- Role: fullstack (login, crud task)
- Challenge: nhieu user edit 1 data -> check ky status, responsive mobile and tablet

** Geometrx - Demographic Data & Reports (USA) | 11/2023 - 12/2023
- Business: tong hop bao cao du lieu dan cu
- Stack: vue, java spring boot, postgre
- Role: frontend (implement components, pages)

** Cost Calculation System (Kyowa Hakko - Japan) | 01/2024 - 02/2024
- Business: quan ly, tinh toan, tong hop bao cao nguyen vat lieu va san pham
- Stack: angular, java spring boot, jpa, oracle
- Role: backend (update reports)

** SPINEX for Energy (Toshiba - Japan) | 03/2024 - 04/2024
- Business: quan ly va giam sat thiet bi
- Stack: react, bootstrap, express, postgre
- Role: frontend (implement components, pages)

** Missing
- English
- Optimize, seo, ui-ux
- Ci-cd: jenkins
- Network, security, hosting
- Webpack, testing


Có khả năng học một ngôn ngữ hay kỹ thuật mới nhanh và vận dụng tốt vào công việc
Có thể hướng dẫn, truyền đạt kiến thức cho các bạn trong nhóm hoặc nhóm khác

** Video Door Phones
- Stack: Linux, Qt Framework, ASP.NET, MS SQL Server

** Home Network System
- Stack: Android, ASP.NET, MS SQL Server


** Jan 2022 - Present

Collaborate with senior developers to build and maintain

Collaborate with team members to build and maintain responsive, single-page applications using ReactJS and related tools such as Ant Design, Mobx.
Write clean, well-documented code and participate in code reviews to ensure high quality and maintainability.
Participate in Agile/Scrum processes and daily stand-up meetings to ensure efficient progress and timely delivery of projects.
Debug and troubleshoot issues and help provide solutions to improve performance and user experience.

Collaborated with a team of developers to build and maintain websites using VueJS, SCSS, TypeScript and other front-end technologies.
Ticketed and resolved bugs and other technical issues reported by clients, and performed cross-browser testing and optimization.
Design and develop intuitive user interfaces by utilizing modern front-end development tools and frameworks.
Write reusable and easily maintainable code, while adhering to industry standards and best practices.
Participate in code reviews and testing to ensure high quality and maintainable code.
Work closely with product managers, designers, and other stakeholders to implement new features and user experiences.
Troubleshoot issues and provide timely solutions to improve application performance and usability.
Develop back-end to integrate front-end functionality with server-side logic.


**
Develops new features, ensures cross browser compatibility
Back-end coding and development

Learning new languages and technologies is what	I am passionate about.
Develop	and fix	the application’s defect base on requirements
Develop small modules of the system
